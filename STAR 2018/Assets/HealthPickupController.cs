﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickupController : MonoBehaviour {

    [Header("Healing")]
    public float healAmount = 50f;
    public FX_Controller healFX;

    [Header("Entice FX")]
    public FX_Controller enticeFX;
    public float enticePulseCooldown = 2f;
    private float enticePulseTimer;
    public float rotationSpeed = 10f;


	
	void Update () {
        transform.Rotate(0f, 0f, rotationSpeed * Time.deltaTime);
        enticePulseTimer += Time.deltaTime;
        if (enticePulseTimer > enticePulseCooldown) {
            enticePulseTimer = 0f;
            enticeFX.PlayAllInstant();
        }
	}

    void OnTriggerEnter2D (Collider2D other) {

        if (other.GetComponent<EntityBody>() != null) {
            other.GetComponent<EntityBody>().entityData.AddHP(healAmount);
            healFX.PlayAllInstant();
            foreach (ObjectDetacher detacher in GetComponentsInChildren<ObjectDetacher>()) {
                detacher.DetachAndScheduleDeath(5f);
            }
            Destroy(gameObject);
        }

    }

}
