﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public static CameraController instance;

    private Camera cam;

    private float camRangeX = 0F;
    private float camRangeY = 0F;

    private float xMin = 0F;
    private float xMax = 0F;
    private float yMin = 0F;
    private float yMax = 0F;

    private float targetRangeX = 0F;
    private float targetRangeY = 0F;

    private float targetXMin = 0F;
    private float targetXMax = 0F;
    private float targetYMin = 0F;
    private float targetYMax = 0F;

    public float orthographicPlayerSize = 5F;
    private List<PlayerData> playersToFrame = new List<PlayerData>();

    public float zoomSpeed = 5F;
    public float followSpeed = 8F;

    void Awake () {
        instance = this;
        cam = GetComponentInChildren<Camera>();
        camRangeY = cam.orthographicSize;
        camRangeX = cam.orthographicSize * Screen.width / Screen.height;
    }

    void Update () {

        playersToFrame.Clear();
        foreach (PlayerData player in PlayerManager.instance.ActivePlayers())
            playersToFrame.Add(player);

        if (playersToFrame.Count > 0) {

            UpdateCurBounds();
            UpdateTargetBounds();
            ScaleToTarget();
            MoveToTarget();

        }
    }

    void UpdateCurBounds () {
        camRangeY = cam.orthographicSize;
        camRangeX = cam.orthographicSize * Screen.width / Screen.height;
        xMin = transform.position.x + camRangeX * -0.5F;
        xMax = transform.position.x + camRangeX * 0.5F;
        yMin = transform.position.y + camRangeY * -0.5F;
        yMax = transform.position.y + camRangeY * 0.5F;
    }

    void UpdateTargetBounds () {
        targetXMax = Mathf.NegativeInfinity; targetXMin = Mathf.Infinity;
        targetYMax = Mathf.NegativeInfinity; targetYMin = Mathf.Infinity;
        foreach (PlayerData player in playersToFrame) {
            if (player.playerObject.transform.position.x + orthographicPlayerSize * Screen.width / Screen.height > targetXMax)
                targetXMax = player.playerObject.transform.position.x + orthographicPlayerSize * Screen.width / Screen.height;
            if (player.playerObject.transform.position.x - orthographicPlayerSize * Screen.width / Screen.height < targetXMin)
                targetXMin = player.playerObject.transform.position.x - orthographicPlayerSize * Screen.width / Screen.height;
            if (player.playerObject.transform.position.y + orthographicPlayerSize > targetYMax)
                targetYMax = player.playerObject.transform.position.y + orthographicPlayerSize;
            if (player.playerObject.transform.position.y - orthographicPlayerSize < targetYMin)
                targetYMin = player.playerObject.transform.position.y - orthographicPlayerSize;
        }
        targetRangeX = targetXMax - targetXMin;
        targetRangeY = targetYMax - targetYMin;
    }

    void ScaleToTarget () {
        float scalePower = targetRangeX / camRangeX;
        if (targetRangeY / camRangeY > scalePower) scalePower = targetRangeY / camRangeY;
        cam.orthographicSize = cam.orthographicSize * (1F + (scalePower * 0.5F - 1F) * Time.deltaTime * zoomSpeed);
    }

    void MoveToTarget () {
        Vector2 targetMidpoint = new Vector2((targetXMin + targetXMax) * 0.5F, (targetYMin + targetYMax) * 0.5F);
        transform.Translate((targetMidpoint - (Vector2)transform.position) * Time.deltaTime * followSpeed);
    }
    
}
