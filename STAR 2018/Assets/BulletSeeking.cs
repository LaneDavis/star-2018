﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SuperFunctions;

public class BulletSeeking : MonoBehaviour {

    public enum SeekingTarget { player, enemy };
    public SeekingTarget seekingTarget = SeekingTarget.player;

    public float seekingPower;
    public float decayRate;

    private float tickTime = 0.25f;
    private float tickTimer = 0f;

    private GameObject targetObject = null;

    void Start () {
        Tick();
    }

    void Update () {

        tickTimer += Time.deltaTime;
        if (tickTimer > tickTime) Tick();

        if (targetObject != null) {
            SteerTowardTarget();
        }

        seekingPower = GameMath.DecayFloat(seekingPower, decayRate, Time.deltaTime);

    }

    void Tick () {

        if (seekingTarget == SeekingTarget.player) {
            PlayerData nearestPlayer = PlayerManager.instance.NearestPlayerToPoint(transform.position);
            if (nearestPlayer != null) {
                targetObject = nearestPlayer.playerObject;
            } else {
                targetObject = null;
            }
        }

    }

    void SteerTowardTarget () {
        transform.Rotate(0f, 0f, Time.deltaTime * seekingPower * (GameMath.isClockwise(transform.right, VecToTarget()) ? 1f : -1f));
    }

    private Vector2 VecToTarget () {
        if (targetObject == null) return Vector2.zero;
        return targetObject.transform.position - transform.position;
    }

}
