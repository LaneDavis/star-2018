﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NecromancerController : BasicEnemyController {

    [Header("Summoning")]
    public GameObject minionPrefab;
    List<EntityData> activeMinions = new List<EntityData>();
    public float minionSpawnCooldown = 5F;
    private float minionSpawnTimer = 0F;

    protected override void Start () {

        base.Start();
        GetComponent<DeathController>().OnDeath += KillAllMinionsOnDeath;

    }

	protected override void Update () {     //This overrides the Update function in BasicEnemyController, which means this happens INSTEAD of that Update().

        base.Update();      //This tells the necromancer to do the base Update (in otherwords, the update function of BasicEnemyController), so it mostly behaves like a normal enemy.
        
        if (targetPlayer != null) {     //Let's not spawn minions when no player is around

            minionSpawnTimer += entityData.EDT;
            if (minionSpawnTimer > minionSpawnCooldown) {
                if (Random.Range(0F, 1F) < 0.5F) minionSpawnTimer = 0F;
                else minionSpawnTimer = 0.75F * minionSpawnTimer;
                SpawnMinion();
                if (name == "Hard Mode Necromancer") SpawnFarMinion();
            }

        }

        entityData.timescale = 2F - entityData.hpCur / entityData.hpMax;

    }

    void SpawnMinion () {

        float minionSpawnDist = Random.Range(1.5f, 3f);
        GameObject newMinion = GameObject.Instantiate(minionPrefab, (Vector2)transform.position + Random.insideUnitCircle.normalized * minionSpawnDist, Quaternion.identity, null);
        EntityData newEntityData = newMinion.GetComponent<EntityData>();
        activeMinions.Add(newEntityData);
        newMinion.GetComponent<DeathController>().OnDeath += RemoveFromActiveMinions;

    }

    void SpawnFarMinion () {

        Vector2 vectorToTargetPlayer = targetPlayer.transform.position - transform.position;
        GameObject newMinion = GameObject.Instantiate(minionPrefab, (Vector2)transform.position + vectorToTargetPlayer + vectorToTargetPlayer.normalized * 20F + Random.insideUnitCircle * 8F, Quaternion.identity, null);
        EntityData newEntityData = newMinion.GetComponent<EntityData>();
        activeMinions.Add(newEntityData);
        newMinion.GetComponent<DeathController>().OnDeath += RemoveFromActiveMinions;

    }

    void RemoveFromActiveMinions (EntityData dyingEntity) {
        if (this != null && activeMinions.Contains(dyingEntity)) {
            activeMinions.Remove(dyingEntity);
        }
    }

    void KillAllMinionsOnDeath (EntityData entityData) {
        for (int i = activeMinions.Count - 1; i >= 0; i--) {
            if (activeMinions[i] != null)
                activeMinions[i].deathController.Die();
        }
    }

}
