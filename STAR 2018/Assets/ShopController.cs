﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShopController : MonoBehaviour {

    public GameObject SoldObjectPrefab;
    public int Price;
    public List<RectTransform> ShopRingRects = new List<RectTransform>();
    public List<RawImage> ShopRingImages = new List<RawImage>();

    public Gradient ShopRingColors;
    public float GradientSpeed;
    public float GradientOffset;
    private float gradientTimer;

    public float SpinSpeed;
    public float SpinMultiplierPerRing;

    public TextMeshProUGUI ShopText;

    private enum ShopObjectType { none, gun, power, health };
    private ShopObjectType objectType = ShopObjectType.none;

    public Color GunColor;
    public Color PowerColor;
    public Color HealthColor;
    public Color GoldColor;

    public Animator Animator;
    public SoundCall ErrorSound;

    void Start () {

        string typeString = "[None]";
        if (SoldObjectPrefab.GetComponent<Gun>() != null) {
            objectType = ShopObjectType.gun;
            typeString = "<color=#" + ColorUtility.ToHtmlStringRGB(GunColor) + ">[Gun] </color>";
        }
        else if (SoldObjectPrefab.GetComponent<Power>() != null) {
            objectType = ShopObjectType.power;
            typeString = "<color=#" + ColorUtility.ToHtmlStringRGB(PowerColor) + ">[Power] </color>";
        }
        else if (SoldObjectPrefab.GetComponent<HealthPickupController>() != null) {
            objectType = ShopObjectType.health;
            typeString = "<color=#" + ColorUtility.ToHtmlStringRGB(HealthColor) + ">[Health] </color>";
        }

        string costString = "<color=#" + ColorUtility.ToHtmlStringRGB(GoldColor) + ">" + Price + "g </color>";

        ShopText.text = typeString + " " + SoldObjectPrefab.name + "\n" + costString;

    }

    void Update () {

        //Update Gradient
        gradientTimer += GradientSpeed * Time.deltaTime;
        for (int i = 0; i < ShopRingImages.Count; i++) {
            ShopRingImages[i].color = ShopRingColors.Evaluate((gradientTimer + GradientOffset * i) % 1f);
        }

        //Update Rotation
        for (int i = 0; i < ShopRingRects.Count; i++) {
            ShopRingRects[i].Rotate(0f, 0f, Time.deltaTime * SpinSpeed * Mathf.Pow(SpinMultiplierPerRing, i));
        }

    }

    //Called by Animator
    public void DestroySelf () {
        Destroy(gameObject);
    }

    void OnTriggerEnter2D (Collider2D other) {
        Debug.Log("OnTriggerEnter2D");
        if (other.transform.parent.GetComponent<PlayerController>() != null && MoneyManager.Instance.TrySpendGold(Price)) {
            AwardObject(other);
            Animator.Play("Success");
        } else {
            SoundManager.thisSoundManager.PlaySound(ErrorSound, gameObject);
        }
    }

    void AwardObject (Collider2D other) {

        if (objectType == ShopObjectType.gun) {
            other.transform.parent.GetComponent<PlayerController>().EquipNewGun(SoldObjectPrefab);
        } else if (objectType == ShopObjectType.power) {
            other.transform.parent.GetComponent<PlayerController>().EquipNewPower(SoldObjectPrefab);
        } else if (objectType == ShopObjectType.health) {
            Instantiate(SoldObjectPrefab, transform.position, Quaternion.identity, null);
        }

    }

}
