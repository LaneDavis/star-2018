﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityBody : MonoBehaviour {

	[HideInInspector] public EntityData entityData;
    [HideInInspector] public DamageTakenProcessor damageTakenProcessor;     //Set Externally by EntityData.

    public AnimationCurve traumaByPercentOfHP = new AnimationCurve(new Keyframe(0F, 0F, 5f, 5f), new Keyframe(0.25f, 1f));

    public FX_Controller damageTraumaFX;

    public void Setup () {      //Called by EntityData AFTER damageTakenProcessor has been set.

        damageTakenProcessor.OnDeliverFinalDamage += IllustrateDamage;

    }

    public void TakeDamage (DamageInstance damageInstance) {

        damageTakenProcessor.ProcessDamage(damageInstance);

    }

    public void IllustrateDamage (DamageInstance damageInstance) {

        float trauma = traumaByPercentOfHP.Evaluate(damageInstance.amount / entityData.hpMax);
        damageTraumaFX.PlayAllInstant(instantMultiplier:trauma);

    }

}

    /*
    EntityBody has two jobs.

    First, it translates bullet impacts into damage taken by sending it to the DamageTakenProcessor.

    Second, it updates the visual state of the player body, causing the sprite to wobble, flash, and pulse
    based on incoming damage or healing.
    */
