﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour {

    public static TerrainGenerator instance;

    public GameObject rockPrefab;

    [Header("World Size")]
    [Range (10F, 100F)] public float worldRadius;

    [Header("Internal Terrain")]
    [Range (0F, 1F)] public float terrainThickness;
    public AnimationCurve thicknessByDistance;
    public AnimationCurve formationScatterCurve;

    List<GameObject> formationRocks = new List<GameObject>();

    void Awake () {
        instance = this;
    }

    void Start () {

        GenerateWorldDisc();
        GenerateInternalTerrain();

	}

    void GenerateWorldDisc () {

        float degreesSoFar = 0f;
        float degreesIncrement = 100f / worldRadius;
        float randomizedIncrement = 0f;
        Vector2 dir = Vector2.up;
        Vector2 placementPos = Vector2.zero;
        GameObject newRock = null;

        while (degreesSoFar < 360f) {
            randomizedIncrement = degreesIncrement * Random.Range(0.8f, 1.25f);
            dir = SuperFunctions.GameMath.RotateVector(dir, randomizedIncrement);

            placementPos = dir * (worldRadius + Random.Range(-1f, 1f));
            newRock = GameObject.Instantiate(rockPrefab, placementPos, Quaternion.identity, transform);
            newRock.transform.localScale *= Random.Range(3F, 5F);

            degreesSoFar += randomizedIncrement;
        }

    }

    void GenerateInternalTerrain () {

        float area = Mathf.Pow(worldRadius, 2f) * Mathf.PI;
        int numFormations = (int)(area * 0.1f * terrainThickness);

        Vector2 placementPos = Vector2.zero;
        GameObject newRock = null;

        //Generate initial formation seeds
        for (int i = 0; i < numFormations; i++) {
            placementPos = Random.insideUnitCircle;
            if (Random.Range(0f, 1f) > thicknessByDistance.Evaluate(placementPos.magnitude)) //Use the thicknessByDistance curve to make it unlikely to place a formation next to the origin.
                continue;
            placementPos *= worldRadius;
            newRock = GameObject.Instantiate(rockPrefab, placementPos, Quaternion.identity, transform);
            newRock.transform.localScale *= Random.Range(1.5F, 3F);
            formationRocks.Add(newRock);
        }

        int scatterNumRandomizer = 0;

        //Generate additional scattered rocks around the formations
        for (int i = 0; i < formationRocks.Count; i++) {
            scatterNumRandomizer = Random.Range(0, 8);
            for (int j = 0; j < scatterNumRandomizer; j++) {
                placementPos = (Vector2)formationRocks[i].transform.position + Random.insideUnitCircle.normalized * formationScatterCurve.Evaluate(Random.Range(0f, 1f));
                newRock = GameObject.Instantiate(rockPrefab, placementPos, Quaternion.identity, transform);
                newRock.transform.localScale *= Random.Range(0.75F, 1.25F);
            }
        }

    }

}
