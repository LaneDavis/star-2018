﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class WallOutlineMatcher : MonoBehaviour {

    public Transform parentTransform;
    public float outlineSize;
    private Vector3 myNewScale = new Vector3 (1f, 1f, 1f);

	void Update () {

        if (parentTransform != null) {
            myNewScale.x = 1f + outlineSize / parentTransform.localScale.x;
            myNewScale.y = 1f + outlineSize / parentTransform.localScale.y;
            transform.localScale = myNewScale;
        }

    }

}
