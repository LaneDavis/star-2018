﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

    public BulletData bulletData;
    public List<ObjectDetacher> detachedOnDeath;
    public List<ParticleSystem> impactParticles;

	void Update () {

        //Move the Bullet
        transform.Translate(Vector2.right * bulletData.speed * Time.deltaTime);

        //Expire the Bullet if it's moved past its range
        if (Time.time - bulletData.birthTime > (bulletData.range / bulletData.speed)) {
            ExpireBullet();
        }

        //Track the Source
        if (bulletData.owner != null) bulletData.ownerLastPosition = bulletData.owner.transform.position;

	}

    void OnTriggerEnter2D (Collider2D other) {

        if (other.gameObject.layer == 12) {         //12 is the "Terrain" layer.
            ExpireBullet();
        }

        else if (other.gameObject.layer == 13) {    //13 is the "DestructibleTerrain" layer.
            EntityBody body = other.GetComponent<EntityBody>();
            if (body == null) return;   //Did we hit an Entity? No? Then who cares! Keep going!
            body.TakeDamage(BuildDamageInstance(body));
            ExpireBullet();
        }

        else {
            EntityBody body = other.GetComponent<EntityBody>();
            if (body == null) return;   //Did we hit an Entity? No? Then who cares! Keep going!
            if (body.entityData.IsHurtByPlayer(bulletData.ownerPlayerNumber)) {
                body.TakeDamage(BuildDamageInstance(body));
                ExpireBullet();
            }
        }

    }

    public DamageInstance BuildDamageInstance (EntityBody body) {
        DamageInstance newInstance = new DamageInstance(bulletData.damage, bulletData.ownerLastPosition, transform.position,
            bulletData.owner, body.entityData.gameObject, bulletData.ownerPlayerNumber,
            body.entityData.playerData == null ? -1 : body.entityData.playerData.playerNumber, //This argument is set up as a "ternary" statement. If the struck body has no playerData, we return -1. Otherwise, we return the playerNumber on its playerData.
            bulletData.buffsApplied
        );     
        return newInstance;
    }

    public void ExpireBullet () {

        foreach (ParticleSystem p in impactParticles) {
            p.Emit((int)p.emission.GetBurst(0).count.constant);
        }
        foreach (ObjectDetacher o in detachedOnDeath) {
            o.DetachAndScheduleDeath(1F);
        }
        Destroy(gameObject);

    }

}
