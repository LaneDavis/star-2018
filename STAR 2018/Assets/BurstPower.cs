﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurstPower : Power {

    public Gun gun;

    override protected void Activate () {

        base.Activate();
        gun.gameObject.SetActive(true);
        gun.Shoot(Vector2.up);

    }

}
