﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityHealthIndicator : MonoBehaviour {

    public SpriteRenderer ownerSprite;
    public SpriteRenderer missingHealthSprite;
    public SpriteMask spriteMask;
    public Transform maskTransform;

    public static int orderCycle = 5;

	void Awake () {
        missingHealthSprite.color = new Color(ownerSprite.color.r / 2f, ownerSprite.color.g / 2f, ownerSprite.color.b / 2f);
        ownerSprite.sortingOrder = orderCycle;
        missingHealthSprite.sortingOrder = orderCycle + 1;
        spriteMask.frontSortingOrder = orderCycle + 2;
        spriteMask.backSortingOrder = orderCycle;
        orderCycle += 3;
    }

    public void SetHealthPercent (float percent) {
        percent = Mathf.Clamp01(percent);
        maskTransform.localPosition = new Vector2(0f, -1f + percent);
    }

}
