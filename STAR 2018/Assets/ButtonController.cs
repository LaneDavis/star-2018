﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : MonoBehaviour {

    [HideInInspector]
    public bool IsOn = false;

    private int bodyCount = 0;

    void OnTriggerEnter2D (Collider2D other) {
        bodyCount++;
        IsOn = true;
    }

    void OnTriggerExit2D (Collider2D other) {
        bodyCount--;
        if (bodyCount <= 0) {
            IsOn = false;
        }
    }

}
