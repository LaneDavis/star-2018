﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Preset", menuName = "Bullets/Bullet Appearance")]
public class BulletAppearancePreset : ScriptableObject {

    //IDENTITY
    public bool oneShot = false;

    //SIZE
    [Header("Size")]
    public AnimationCurve sizeCurve;

    //GRADIENT
    [Header("Gradient")]
    public Gradient gradient;
    public float gradientTime = 1F;
    public bool gradientLoops = true;
    public bool randomGradientStartTime;

}
