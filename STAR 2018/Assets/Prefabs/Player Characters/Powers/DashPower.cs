﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashPower : Power {

    public BuffPreset DashBuffPreset;

    override protected void Activate () {

        base.Activate();
        entityData.buffController.AddBuff(DashBuffPreset);

    }

}
