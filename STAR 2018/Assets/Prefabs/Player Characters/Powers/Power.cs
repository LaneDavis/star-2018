﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Power : MonoBehaviour {

    [HideInInspector] public EntityData entityData;

    [Header("Timing")]
    public float cooldownTime;
    private float cooldownTimeLeft;

    [Header("FX")]
    public FX_Controller activationFx;

    virtual public void TryActivate () {

        if (cooldownTimeLeft <= 0f) {
            cooldownTimeLeft = cooldownTime;
            Activate();
        }

    }

    virtual protected void Activate () {

        cooldownTimeLeft = cooldownTime;
        if (activationFx != null) {
            activationFx.PlayAllInstant(entityData.gameObject);
        }

    }

    virtual public void Update () {

        if (cooldownTimeLeft > 0) {
            cooldownTimeLeft -= Time.deltaTime;
        }

    }

}
