﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Character Class", menuName = "Classes/Character Class")]
public class CharacterClass : ScriptableObject {

    [Header("Name")]
    public string className;

    [Header("Appearance")]
    public Texture cardPortrait;
    public Color textColor;

    [Header("Stats")]
    public float maxHP = 200f;
    public float moveSpeed = 9f;
    public float size = 1f;

    [Header("Starting Guns")]
    public List<GameObject> gunPrefabs;

    [Header("Powers")]
    public List<GameObject> powerPrefabs;

}
