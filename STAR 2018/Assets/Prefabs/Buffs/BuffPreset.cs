﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Preset", menuName = "Buffs/Buff Preset")]
public class BuffPreset : ScriptableObject {

    //EFFECTS
    [Header("Effects")]
    public List<BuffEffect> effects = new List<BuffEffect>();

    //LIFETIME
    [Header("Lifetime")]
    public float duration;
    public bool fades = true;

    //VFX & SFX
    [Header("FX")]
    public GameObject FXPrefab = null;

}
