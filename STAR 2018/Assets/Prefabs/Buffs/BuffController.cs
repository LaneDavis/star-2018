﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffController : MonoBehaviour {

    [HideInInspector] public EntityData EntityData;

    //BUFF TYPES
    public enum BuffTypes { dmgDealt, dmgTaken, fireRate, moveSpeed, regen  }

    //BUFF VALUES — Other entity components look at these values to multiply their damage, movement, etc.
    [HideInInspector] public float dmgDealt = 1f;     //Multiplier on the damage this character deals
    [HideInInspector] public float dmgTaken = 1f;     //Multiplier on the damage this character takes.
    [HideInInspector] public float fireRate = 1f;     //Multiplier on how fast this character shoots with all guns.
    [HideInInspector] public float moveSpeed = 1f;    //Multiplier on how fast this character moves.
    [HideInInspector] public float regen = 0f;        //Flat value that represents HP healed per second.

    [HideInInspector] public List<Buff> activeBuffs = new List<Buff>();
    
    private List<Buff> buffsToEnd = new List<Buff>();
    private List<float> positiveFactors = new List<float>();
    private List<float> negativeFactors = new List<float>();
    private float buffValueSoFar;

    void Update () {

        //Update Buff Values
        dmgDealt = UpdateMultipliedBuff(BuffTypes.dmgDealt, 1f);
        dmgTaken = UpdateMultipliedBuff(BuffTypes.dmgTaken, 1f);
        fireRate = UpdateMultipliedBuff(BuffTypes.fireRate, 1f);
        moveSpeed = UpdateMultipliedBuff(BuffTypes.moveSpeed, 1f);
        regen = UpdateAddedBuff(BuffTypes.regen, 0f);
        
        //Update Buff Ages & Expire Old Buffs
        for (int i = activeBuffs.Count - 1; i >= 0; i--) {
            activeBuffs[i].age += Time.deltaTime;
            if (activeBuffs[i].age > activeBuffs[i].preset.duration) {
                if (activeBuffs[i].FXController != null) activeBuffs[i].FXController.ScheduleDeath(0f);
                activeBuffs.RemoveAt(i);
            }
        }

	}

    float UpdateMultipliedBuff (BuffTypes type, float defaultValue) {
        positiveFactors.Clear(); negativeFactors.Clear();
        buffValueSoFar = defaultValue;
        for (int i = 0; i < activeBuffs.Count; i++) {
            for (int j = 0; j < activeBuffs[i].preset.effects.Count; j++) {
                if (activeBuffs[i].preset.effects[j].buffType == type) {
                    if (activeBuffs[i].preset.effects[j].value > 0) positiveFactors.Add(activeBuffs[i].preset.effects[j].value * activeBuffs[i].TimeMultiplier());
                    else negativeFactors.Add(activeBuffs[i].preset.effects[j].value * activeBuffs[i].TimeMultiplier());
                }
            }
        }
        for (int i = 0; i < positiveFactors.Count; i++) {
            buffValueSoFar += positiveFactors[i];
        }
        for (int i = 0; i < negativeFactors.Count; i++) {
            buffValueSoFar *= (1f + negativeFactors[i]);
        }
        return buffValueSoFar;
    }

    float UpdateAddedBuff (BuffTypes type, float defaultValue) {
        buffValueSoFar = defaultValue;
        for (int i = 0; i < activeBuffs.Count; i++) {
            for (int j = 0; j < activeBuffs[i].preset.effects.Count; j++) {
                if (activeBuffs[i].preset.effects[j].buffType == type) {
                    buffValueSoFar += activeBuffs[i].preset.effects[j].value * activeBuffs[i].TimeMultiplier();
                }
            }
        }
        return buffValueSoFar;
    }

    public Buff AddBuff (BuffPreset buffPreset, GameObject creatorObject = null, PlayerData creatorPlayer = null) {

        //First, we check whether this character already has that buff.
        Buff matchingBuff = null;
        for (int i = 0; i < activeBuffs.Count; i++) {
            if (activeBuffs[i].preset == buffPreset) {
                matchingBuff = activeBuffs[i];
                break;
            }
        }

        //If there is a copy of that buff, we'll refresh the duration of that buff.
        if (matchingBuff != null) {
            matchingBuff.age = 0f;
            if (matchingBuff.FXController != null)
                matchingBuff.FXController.ResetElapsedTime();
            return matchingBuff;
        }

        //Otherwise, since this character doesn't already have that buff, we'll give them a fresh copy of it.
        else {
            Buff newBuff = new Buff(buffPreset, creatorObject, creatorPlayer);
            if (buffPreset.FXPrefab != null) {
                GameObject FXObject = GameObject.Instantiate(buffPreset.FXPrefab, transform.position, Quaternion.identity, transform);
                newBuff.FXController = FXObject.GetComponent<FX_Controller>();
                newBuff.FXController.anchorObject = gameObject;
            }
            activeBuffs.Add(newBuff);
            return newBuff;
        }

    }

    public void ApplyBuffsFromDamageSource (DamageInstance damageInstance) {
        if (damageInstance != null && damageInstance.buffsApplied != null) {
            for (int i = 0; i < damageInstance.buffsApplied.Count; i++) {
                AddBuff(damageInstance.buffsApplied[i]);
            }
        }
    }

    public List<DamageInstance> GetDamageOverTimeInstances () {
        List<DamageInstance> list = new List<DamageInstance>();
        for (int i = 0; i < activeBuffs.Count; i++) {
            for (int j = 0; j < activeBuffs[i].preset.effects.Count; j++) {
                if (activeBuffs[i].preset.effects[j].buffType == BuffTypes.regen && activeBuffs[i].preset.effects[j].value < 0) {
                    DamageInstance newDamageInstance = new DamageInstance(-activeBuffs[i].preset.effects[j].value);
                    newDamageInstance.impactPosition = transform.position;
                    newDamageInstance.receiverObject = gameObject;
                    if (EntityData.playerData != null) {
                        newDamageInstance.receiverPlayerNumber = EntityData.playerData.playerNumber;
                    }
                    if (activeBuffs[i].creatorObject != null) {
                        newDamageInstance.ownerObject = activeBuffs[i].creatorObject;
                        newDamageInstance.ownerLastPosition = activeBuffs[i].creatorObject.transform.position;
                    }
                    if (activeBuffs[i].creatorPlayer != null) {
                        newDamageInstance.ownerPlayerNumber = activeBuffs[i].creatorPlayer.playerNumber;
                    }
                    list.Add(newDamageInstance);
                }
            }
        }
        return list;
    }

}

public class Buff {

    public BuffPreset preset;
    public float age;
    public GameObject creatorObject;
    public PlayerData creatorPlayer;
    public FX_Controller FXController;

    public Buff (BuffPreset buffPreset, GameObject creatorObject = null, PlayerData creatorPlayer = null) {
        preset = buffPreset;
        this.creatorObject = creatorObject;
        this.creatorPlayer = creatorPlayer;
    }
    
    public float TimeMultiplier () {
        if (preset.fades) return (Mathf.Clamp01(1f - age / preset.duration));
        else return 1f;
    }

}

[System.Serializable]
public class BuffEffect {

    public BuffController.BuffTypes buffType;
    public float value = 0f;

}