﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletData : MonoBehaviour {

	[Range(0.1f, 100f)] public float damage = 5f;
	[Range(0.1f, 40f)] public float speedMin = 10f;
	[Range(0.1f, 40f)] public float speedMax = 10f;
    [HideInInspector] public float speed;
	[Range(0.1f, 100f)] public float range = 100f;
    
    [HideInInspector] public float birthTime = 0f;
    
    [HideInInspector] public int ownerPlayerNumber = -1;            //-1 is the player number of AI enemies. If this is shot by a player, this number will be replaced with the player's number.
    [HideInInspector] public GameObject owner = null;
    [HideInInspector] public Vector2 ownerLastPosition = Vector2.zero;

    public List<BuffPreset> buffsApplied = new List<BuffPreset>();

    void Awake () {
        speed = Random.Range(speedMin, speedMax);
    }

}
