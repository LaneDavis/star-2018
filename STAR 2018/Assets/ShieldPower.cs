﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldPower : Power {

    public BuffPreset ShieldBuffPreset;

    override protected void Activate () {

        base.Activate();
        entityData.buffController.AddBuff(ShieldBuffPreset);

    }

}