﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CultLeaderController : BasicEnemyController {

    [Header("Cult Leader Parameters")]
    public float phaseLength = 8f;
    public float phaseTimer = 0f;
    public AnimationCurve speedRamp;
    private int activeGun = 0;
    public GameObject cultistPrefab;

    protected override void FirstUpdate () {

        base.FirstUpdate();

        for (int i = 0; i < entityData.guns.Count; i++) {
            entityData.guns[i].gameObject.SetActive(i == 0);
        }

    }

    protected override void Update () {  //This function is "virtual" which means it can be overridden in child scripts. To see this in action, check out the NecromancerController script

        base.Update();

        phaseTimer += entityData.EDT;
        if (phaseTimer > phaseLength) {
            phaseTimer = 0f;
            SwitchToNextGun();
        }

        entityData.timescale = speedRamp.Evaluate(SpeedRampProgress());

    }

    private void SwitchToNextGun () {

        int numGuns = entityData.guns.Count;
        activeGun++;
        if (activeGun >= numGuns) {
            activeGun = 0;
        }
        for (int i = 0; i < entityData.guns.Count; i++) {
            entityData.guns[i].gameObject.SetActive(i == activeGun);
        }

        if (activeGun == 0) {
            for (int i = 0; i < 3; i++) {
                GameObject.Instantiate(cultistPrefab, (Vector2)transform.position + SuperFunctions.GameMath.RotateVector(Vector2.right, (float)i * 120f), Quaternion.identity, null);
            }
        }

    }

    private float SpeedRampProgress () {
        return 1f - entityData.hpCur / entityData.hpMax;
    }

}
