﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageTakenProcessor : MonoBehaviour {

    [HideInInspector] public EntityData entityData;
    public delegate void DamageTaken(DamageInstance damageInstance);
    public event DamageTaken OnDeliverFinalDamage;

    public DamageInstance ProcessDamage (DamageInstance damageInstance) {

        //TODO: Buff Processing.
        //TODO: Shield Processing.

        if (OnDeliverFinalDamage != null) OnDeliverFinalDamage(damageInstance);
        return damageInstance;

    }

}

    /*
    You might be thinking "what on earth does a DamageTakenProcessor do?!"

    Well, a LOT of different parts of a player/enemy care about damage.
    You have shields and buffs that can interfere with the amount of damage actually taken.
    Then, you have passive abilities that react based on the damage that actually comes through.
    Finally, you have special effects like shake and particles that trigger when damage is taken.

    This script provides a single location where we can start with an amount of damage,
    then run that damage through ALL the different things that affect damage taken.
    Finally, we use an event called OnDamageTaken to pass a message on to everything that
    cares about taking damage to let it know how much damage we just took.

    So the really new thing here is the delegate and event. The key is that we can tell other
    scripts to "listen" to the OnDamageTaken event, so that they can do something whenever
    damage is taken. It'll look like this:

    damageTakenProcessor.OnDamageTaken += DamageTakenFunction

    Where DamageTakenFunction is something that happens inside the other script. For an example, take
    a look at PlayerController, which is the part that updates the player's current HP.
    */
