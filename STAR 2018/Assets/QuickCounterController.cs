﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

public class QuickCounterController : MonoBehaviour {

    [Header("Time")]
    public TextMeshProUGUI TimerText;

    [Header("Gold")]
    public TextMeshProUGUI GoldText;

    [Header("Enemies")]
    public TextMeshProUGUI EnemiesText;

    [Header("Seals")]
    public TextMeshProUGUI SealsText;
    public List<Gate> SealGates = new List<Gate>();
    private int gatesLeft;

    [Header("Boss")]
    public TextMeshProUGUI BossText;
    public GameObject Boss;

    void Update () {

        if ((!EnemiesText.IsActive() || EnemyManager.instance.activeEnemies.Count <= 0) && (!BossText.IsActive() || Boss == null)) {
            TimerText.color = Color.green;
        }
        else {
            TimerText.text = TimerString();
        }

        if (GoldText.gameObject.activeSelf) {
            GoldText.text = GoldString();
        }

        if (EnemiesText.gameObject.activeSelf) {
            EnemiesText.text = EnemiesString();
        }

        if (SealsText.gameObject.activeSelf) { 
            SealsText.text = SealsString();
            if (gatesLeft == 0) {
                SealsText.color = Color.green;
            }
        }

        if (BossText.gameObject.activeSelf) {
            BossText.text = BossString();
            if (Boss == null) {
                BossText.color = Color.green;
            }
        }

    }

    public string TimerString () {

        float timeLeft = Time.time;
        int minutes = (int)timeLeft / 60;
        timeLeft -= (float)minutes * 60;
        float afterMinutes = timeLeft;

        return ("Time: " + minutes.ToString() + ":" + (afterMinutes < 10f ? "0" : "") + afterMinutes.ToString("F2"));
        
    }

    public string GoldString () {

        return ("Gold: " + MoneyManager.Instance.gold);

    }

    public string EnemiesString () {

        return ("Enemies: " + EnemyManager.instance.activeEnemies.Count);

    }

    public string SealsString () {

        gatesLeft = SealGates.Count;
        foreach (Gate gate in SealGates) {
            if (gate.openingPercent > 0.1f) {
                gatesLeft--;
            }
        }
        return ("Seals Left: " + gatesLeft);

    }

    public string BossString () {

        if (Boss == null) return "Boss Slain: Yes";
        else return "Boss Slain: No";

    }

}
