﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour {
    
	[HideInInspector] public EntityData entityData;
    public GameObject bulletPrefab;

    [Range (.01F, 20F)] public float firingRate = 5F;
    private float firingTimer = 0F;

    public FX_Controller shootFX;
    public FX_Controller switchFX;

    [Header("Spread")]
    public int numBullets = 1;
    public float arcDegrees = 0F;
    private float degreesPerBullet = 0F;
    private float firstBulletOffset = 0F;

    [Header("Slowdown")]
    public BuffPreset slowdownDebuff;
    
    public void Shoot (Vector2 dir) {
        if (firingTimer >= (1f/firingRate)) {
            if (slowdownDebuff != null) {
                entityData.buffController.AddBuff(slowdownDebuff, entityData.gameObject, entityData.playerData);
            }
            firingTimer = 0F;
            SpawnBullet(dir);
        }
    }

    void SpawnBullet (Vector2 dir) {

        if (shootFX != null) {
            shootFX.PlayAllInstant();
        }

        //Set up bullet spread
        if (numBullets <= 1) {  //For one bullet, no spread at all.
            degreesPerBullet = 0F;
            firstBulletOffset = 0F;
        }
        else {    //For more bullets...
            degreesPerBullet = arcDegrees / (numBullets - 1);
            firstBulletOffset = -0.5F * arcDegrees;
        }

        for (int i = 0; i < numBullets; i++) {

            GameObject newBullet = GameObject.Instantiate(bulletPrefab, transform.position, transform.rotation, null);
            BulletData newData = newBullet.GetComponent<BulletData>();
            newData.birthTime = Time.time;
            newData.owner = entityData.gameObject;
            newData.ownerLastPosition = entityData.gameObject.transform.position;
            newBullet.transform.Rotate(0F, 0F, 360F - SuperFunctions.GameMath.degreesFromVector(dir));
            if (entityData.playerData != null) {
                newData.ownerPlayerNumber = entityData.playerData.playerNumber;
                newBullet.GetComponent<SpriteRenderer>().color = entityData.playerData.playerColor;
            }
            newBullet.layer = newData.ownerPlayerNumber == -1 ? 11 : 10;    //Confusing looking line. Assigns this to the physics layer "EnemyBullet" or "PlayerBullet" as appropriate.

            newBullet.transform.Rotate(0F, 0F, (float)i * degreesPerBullet + firstBulletOffset);

        }
    }

    void Update () {
        firingTimer += entityData.EDT * entityData.buffController.fireRate;
    }

}
