﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyManager : MonoBehaviour {

    public static MoneyManager Instance;

    public int gold = 0;

    void Awake () {
        Instance = this;
    }

    public void EarnGold (int amount) {
        gold += amount;
    }

    public bool TrySpendGold (int cost) {
        if (gold < cost) return false;
        gold -= cost;
        return true;
    }

}
