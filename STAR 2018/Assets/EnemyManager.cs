﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour {

    public static EnemyManager instance;

    [Header("Starting Enemies")]
    public GameObject startingEnemyPrefab;
    [Range(0f, 1f)] public float startingEnemyThickness;
    public AnimationCurve startingEnemyDistanceCurve;

    [Header("Hunter Enemies")]
    public GameObject huntingEnemyPrefab;
    public AnimationCurve hunterSpawnRateByTime;
    private float spawnTimer;
    private float totalElapsedTimer;

    [HideInInspector] public List<EntityData> activeEnemies = new List<EntityData>();

    void Awake () {
        instance = this;
    }
    
    void Start () {

        //Un-Comment this section if you're using Terrain Generation

        //float area = TerrainGenerator.instance.worldRadius * TerrainGenerator.instance.worldRadius * Mathf.PI;
        //int numEnemies = (int)(startingEnemyThickness * area * 0.05f);

        //Vector2 spawnPos = Vector2.zero;
        //GameObject newEnemy = null;

        //for (int i = 0; i < numEnemies; i++) {
        //    spawnPos = Random.insideUnitCircle;
        //    if (Random.Range(0f, 1f) > startingEnemyDistanceCurve.Evaluate(spawnPos.magnitude))
        //        continue;
        //    spawnPos *= (TerrainGenerator.instance.worldRadius - 5F);
        //    newEnemy = GameObject.Instantiate(startingEnemyPrefab, spawnPos, Quaternion.identity, null);
        //}

    }

    void Update () {

        //Un-Comment this section if you're using Terrain Generation

        //totalElapsedTimer += Time.deltaTime;
        //spawnTimer += hunterSpawnRateByTime.Evaluate(totalElapsedTimer) * Time.deltaTime;
        //if (spawnTimer >= 1) {
        //    Vector2 spawnPos = Random.insideUnitCircle * (TerrainGenerator.instance.worldRadius - 5F);
        //    if (PointOkayToSpawn(spawnPos)) {
        //        spawnTimer = 0;
        //        GameObject.Instantiate(huntingEnemyPrefab, spawnPos, Quaternion.identity, null);
        //    }
        //}

    }
    
    public void AddEnemy (EntityData newEntityData) {
        activeEnemies.Add(newEntityData);
    }

    public void RemoveEnemy (EntityData entityDataToRemove) {
        if (activeEnemies.Contains(entityDataToRemove)) {
            activeEnemies.Remove(entityDataToRemove);
        }
    }

    public void AggroEnemiesInRadius (Vector2 point, float radius, GameObject targetPlayer) {
        List<EntityData> EnemiesInRadius = EnemiesWithinRadius(point, radius);
        for (int i = 0; i < EnemiesInRadius.Count; i++) {
            if (EnemiesInRadius[i].enemyController != null && EnemiesInRadius[i].enemyController.targetPlayer == null) {
                EnemiesInRadius[i].enemyController.targetPlayer = targetPlayer;
            }
        }
    }

    public List<EntityData> EnemiesWithinRadius (Vector2 point, float radius) {
        List<EntityData> newList = new List<EntityData>();
        for (int i = 0; i < activeEnemies.Count; i++) {
            if (Vector2.Distance(point, (Vector2)activeEnemies[i].transform.position) < radius)
                newList.Add(activeEnemies[i]);
        }
        return newList;
    }

    private bool PointOkayToSpawn (Vector2 point) {
        if (PlayerManager.instance.NearestPlayerToPoint(point) == null)
                return false;
        if (Vector2.Distance(PlayerManager.instance.NearestPlayerToPoint(point).playerObject.transform.position, point) < 10f)
            return false;
        return true;
    }

}
