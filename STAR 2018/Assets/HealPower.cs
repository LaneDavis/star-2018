﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealPower : Power {

    public float radius = 4f;
    public float healPercent = 0.25f;

    override protected void Activate () {

        base.Activate();
        foreach (PlayerData p in PlayerManager.instance.ActivePlayers()) {

            if (Vector2.Distance((Vector2)transform.position, (Vector2)p.entityData.transform.position) < radius) {
                p.entityData.AddHP(p.entityData.hpMax * healPercent);
            }

        }

    }

}
