﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using TMPro;

public class ClassCardController : MonoBehaviour {

    public Animator animator;
    public RawImage portrait;
    public TextMeshProUGUI className;
    public List<TextMeshProUGUI> classPowers;
    public CharacterClass characterClass;

    public void Setup (CharacterClass characterClass) {
        this.characterClass = characterClass;
        portrait.texture = characterClass.cardPortrait;
        className.text = characterClass.className;
        className.color = characterClass.textColor;
        for (int i = 0; i < classPowers.Count; i++) {
            classPowers[i].color = characterClass.textColor;
            if (characterClass.powerPrefabs.Count > i) {
                classPowers[i].text = characterClass.powerPrefabs[i].name;
            } else {
                classPowers[i].text = "";
            }
        }
    }

    public void SetAnimator (string animationString) {
        animator.Play(animationString);
    }

    public void KillSelf () {

        Destroy(gameObject);

    }

}
