﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ActionSequencer : MonoBehaviour {

    public bool isActive = false;

    private float duration;
    public float timer;
    private List<ActionStep> steps = new List<ActionStep>();

    public ActionSequencer (List<ActionStep> startingSteps = null) {
        if (startingSteps != null) {
            for (int i = 0; i < startingSteps.Count; i++) {
                steps.Add(startingSteps[i]);
                if (startingSteps[i].time > duration) duration = startingSteps[i].time;
            }
        }
    }

    public void ResetTimer () {
        timer = 0F;
    }

    public void AddStep (ActionStep step) {
        steps.Add(step);
        if (step.time > duration) duration = step.time;
    }

    public void AddStep (Action<object> action, float time, object executingScript, object argument = null) {
        steps.Add(new ActionStep(action, time, executingScript, argument));
        if (time > duration) duration = time;
    }

    public void AddNextStep (ActionStep step) {
        step.time += duration;
        steps.Add(step);
        if (step.time > duration) duration = step.time;
    }

    public void AddNextStep (Action<object> action, float time, object executingScript, object argument = null) {
        time += duration;
        steps.Add(new ActionStep(action, time, executingScript, argument));
        if (time > duration) duration = time;
    }

    public void AddEndStep () {
        steps.Add(new ActionStep(EndSequence, duration, this));
    }

    public void ClearSteps () {
        steps.Clear();
    }

    public void RefreshSteps () {
        for (int i = 0; i < steps.Count; i++) steps[i].done = false;
    }

    public void UpdateTime (float deltaTime) {
        if (isActive) {
            timer += deltaTime;
            for (int i = 0; i < steps.Count; i++) {
                if (!steps[i].done && timer > steps[i].time && steps[i].executingScript != null) {
                    steps[i].done = true;
                    steps[i].action(steps[i].argument);
                }
            }
        }
    }

    public void EndSequence (object obj = null) {
        isActive = false;
        ResetTimer();
        RefreshSteps();
    }

    public float StepProgress (Action<object> stepAction, float progressDenominator) {
        if (progressDenominator <= 0F) {
            Debug.LogWarning("StepProgress called with <= 0 denominator");
            return 0F;
        }
        for (int i = 0; i < steps.Count; i++) {
            if (stepAction == steps[i].action) {
                return Mathf.Clamp01((timer - steps[i].time) / progressDenominator);
            }
        }
        return 0F;
    }

    public float Progress () {
        if (duration == 0) return 0F;
        return Mathf.Clamp01(timer / duration);
    }

}

public class ActionStep {

    public Action<object> action;
    public float time;
    public object executingScript;
    public object argument;
    public bool done = false;

    public ActionStep (Action<object> action, float time, object executingScript, object argument = null) {
        this.action = action;
        this.time = time;
        this.executingScript = executingScript;
        this.argument = argument;
    }

}
