﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    /*Ever see a game ability like "you take 50% less damage from fire sources?"

    To do that kind of thing, whenever we tell an entity to take damage, we'll pass along a few
    extra pieces of data along with the damage amount. This way, any abilities like the one above
    can know exactly where and from whom damage was dealt.

    We could also use this kind of thing to save damage sources and compile a "what killed you"
    readout after a player dies.
    */

public class DamageInstance {

    public float amount;
    
    public GameObject ownerObject;      //The owner of the damaging object. Note that a shooter may be dead by the time a bullet hits, so you shouldn't rely on this.
    public GameObject receiverObject;

    //Player Numbers (if appropriate)
    public int ownerPlayerNumber = -1;
    public int receiverPlayerNumber = -1;

    public Vector2 ownerLastPosition = Vector2.zero;
    public Vector2 impactPosition = Vector2.zero;

    public List<BuffPreset> buffsApplied;

    public DamageInstance (float amount, Vector2? ownerLastPosition = null, Vector2? impactPosition = null, GameObject ownerObject = null, GameObject receiverObject = null, int ownerPlayerNumber = -1, int receiverPlayerNumber = -1, List<BuffPreset> buffsApplied = null) {
        this.amount = amount;
        if (ownerLastPosition != null) this.ownerLastPosition = (Vector2)ownerLastPosition;
        if (impactPosition != null) this.impactPosition = (Vector2)impactPosition;
        this.ownerObject = ownerObject;
        this.receiverObject = receiverObject;
        this.ownerPlayerNumber = ownerPlayerNumber;
        this.receiverPlayerNumber = receiverPlayerNumber;
        this.buffsApplied = buffsApplied;
    }

}
