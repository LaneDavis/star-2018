﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour {

    public static CameraShake instance;

    float trauma = 0f;
    public float traumaFallRate = 1f;
    public float translationMax;
    public float rotationMax;
    public float randomSpeed = 10f;

    float xPosPerlinSeed = 1f;
    float yPosPerlinSeed = 5f;
    float rotPerlinSeed = 10f;

    void Awake () {
        instance = this;
    }

    void Update () {

        transform.localPosition = translationMax * Mathf.Pow(trauma, 2f) * new Vector2(0.5f - Mathf.PerlinNoise(Time.time * randomSpeed, xPosPerlinSeed), 0.5f - Mathf.PerlinNoise(Time.time * randomSpeed, yPosPerlinSeed));
        transform.rotation = Quaternion.identity;
        transform.Rotate(0f, 0f, rotationMax * Mathf.Pow(trauma, 2f) * (0.5f - Mathf.PerlinNoise(Time.time * randomSpeed, rotPerlinSeed)));
        trauma = Mathf.Clamp01(trauma - traumaFallRate * Time.deltaTime);

    }

    public void AddTrauma (float traumaAmount) {
        traumaAmount = Mathf.Clamp01(traumaAmount);
        trauma = Mathf.Clamp01(trauma + traumaAmount);
    }

}
