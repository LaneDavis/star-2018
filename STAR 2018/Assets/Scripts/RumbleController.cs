﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

using SuperFunctions;

public class RumbleController : MonoBehaviour {

    [HideInInspector] public EntityData entityData;
    private List<RumbleInstance> rumbleInstances = new List<RumbleInstance>();

    private float highestValueSoFarLeft;
    private float highestValueSoFarRight;

    void Update () {

        if (rumbleInstances.Count == 0)         //If there are zero rumble instance, we might as well skip the whole thing.
            return;

        highestValueSoFarLeft = 0f;
        highestValueSoFarRight = 0f;

        for (int i = rumbleInstances.Count - 1; i >= 0; i--) {
            if (rumbleInstances[i].LeftValue() > highestValueSoFarLeft) {
                highestValueSoFarLeft = rumbleInstances[i].LeftValue();
            }
            if (rumbleInstances[i].RightValue() > highestValueSoFarRight) {
                highestValueSoFarRight = rumbleInstances[i].RightValue();
            }
            rumbleInstances[i].DecaySelf(Time.unscaledDeltaTime);   //We'll decay using unscaledDeltaTime so that rumble fades off even when the game is paused. Otherwise it would last forever!
            if (rumbleInstances[i].value <= 0.01f) {
                rumbleInstances.RemoveAt(i);
            }
        }

        entityData.playerData.player.SetVibration(0, highestValueSoFarLeft);
        entityData.playerData.player.SetVibration(1, highestValueSoFarRight);

    }

    /// <summary>
    /// Adds a new instance of rumble to the player's controller. RumbleController will keep track of all
    /// instances, figure out which one is the highest for each hand, and send that rumble to each hand.
    /// It will also decay rumble instances over time and eliminate any that are too weak to be felt anymore.
    /// </summary>
    /// <param name="rumbleValue">Higher equals more powerful rumble.</param>
    /// <param name="panValue">-1 will only be felt by the left hand, 0 by both, and 1 only by the right hand.</param>
    /// <param name="decayRate">Higher value means rumble ends faster.</param>
    public void AddRumble (float rumbleStrength, float panPosition = 0f, float decayRate = 5f) {

        rumbleStrength = Mathf.Clamp(rumbleStrength, 0f, 1f);
        panPosition = Mathf.Clamp(panPosition, -1f, 1f);
        decayRate = Mathf.Clamp(decayRate, 1f, 12f);
        rumbleInstances.Add(new RumbleInstance(rumbleStrength, panPosition, decayRate));

    }

}

public class RumbleInstance {

    public float value;
    public float panPosition;
    public float decayRate;

    public RumbleInstance (float value, float panPosition, float decayRate) {
        this.value = value;
        this.panPosition = panPosition;
        this.decayRate = decayRate;
    }

    public float LeftValue () {
        return value * Mathf.Pow(-(panPosition - 1f), 0.5f);
    }

    public float RightValue () {
        return value * Mathf.Pow(panPosition + 1f, 0.5f);
    }

    public void DecaySelf (float time) {
        value = GameMath.DecayFloat(value, decayRate, time);
    }

}