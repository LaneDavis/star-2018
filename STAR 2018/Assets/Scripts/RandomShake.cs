﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomShake : MonoBehaviour {

    float trauma = 0f;
    public float traumaFallRate = 1f;
    public float translationMax;
    public float rotationMax;
    public float randomSpeed = 10f;

    float xPosPerlinSeed = 0f;
    float yPosPerlinSeed = 0f;
    float rotPerlinSeed = 0f;

    public bool addTrauma;

    void Awake () {
        xPosPerlinSeed = Random.Range(0f, 50f);
        yPosPerlinSeed = Random.Range(100f, 150f);
        rotPerlinSeed = Random.Range(200f, 250f);
    }

    void Update () {

        if (addTrauma) {
            addTrauma = false;
            AddTrauma(0.4f);
        }

        transform.localPosition = translationMax * Mathf.Pow(trauma, 2f) * new Vector2(0.5f - Mathf.PerlinNoise(Time.time * randomSpeed, xPosPerlinSeed), 0.5f - Mathf.PerlinNoise(Time.time * randomSpeed, yPosPerlinSeed));
        transform.localRotation = Quaternion.identity;
        transform.Rotate(0f, 0f, rotationMax * Mathf.Pow(trauma, 2f) * (0.5f - Mathf.PerlinNoise(Time.time * randomSpeed, rotPerlinSeed)));
        trauma = Mathf.Clamp01(trauma - traumaFallRate * Time.deltaTime);

    }

    public void AddTrauma (float traumaAmount) {
        traumaAmount = Mathf.Clamp01(traumaAmount);
        trauma = Mathf.Clamp01(trauma + traumaAmount);
    }

}
