﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BulletAppearanceController : MonoBehaviour {

    //IDENTITY
    protected BulletController bulletController;
    [HideInInspector] public BulletData bulletData;

    //ACTIVE PRESETS
    [Header("Active Presets")]
    public List<BulletAppearancePreset> autoPresets = new List<BulletAppearancePreset>();
    protected List<BulletAppearance> activePresets = new List<BulletAppearance>();
    protected List<BulletAppearance> presetsToRemove = new List<BulletAppearance>();

    //SIZE
    protected float baseSize = 1F;
    protected float sizeMultiplier_presetAggregate = 1F;

    //COLOR
    protected SpriteRenderer sprite;
    protected Color baseColor;
    //Color Processing Variables
    protected Color colorProcessing_aggregateSoFar;
    protected Color colorProcessing_curPresetColor;
    protected float colorProcessing_gradientAge;
    protected float colorProcessing_weightSoFar;
    protected float colorProcessing_highestOpacitySoFar;
    protected Color colorProcessing_colorToApply;

    //TIME
    protected float tickTime = 0.25F;
    protected float tickTimer = 0F;

    #region Initialization

    void Awake () {

        //Set Up Identity
        if (GetComponent<BulletController>() != null) {
            bulletController = GetComponent<BulletController>();
            bulletData = GetComponent<BulletData>();
        }

        //Get Sprite
        sprite = GetComponent<SpriteRenderer>();

        //Add Auto Presets
        for (int i = 0; i < autoPresets.Count; i++) {
            AddPreset(autoPresets[i]);
        }

        //Get Base Size and Color
       ShooterSetup(); 

        //Immediately set the size and color.
        UpdateSize();
        UpdateColor();

    }

    public void ShooterSetup () {

        //Get Base Size
        baseSize = transform.localScale.magnitude;

        //Get Base Color
        baseColor = sprite.color;

    }

    #endregion
    #region Update

    void Update () {

        //Update Size
        UpdateSize();

        //Update Color
        UpdateColor();

        //Run Tick Timer for Bigger Operations
        tickTimer += Time.deltaTime;
        if (tickTimer > tickTime) { Tick();}

    }

    protected void Tick () {

        tickTimer = 0F;
        CheckOneShotRemoval();

    }

    public void UpdateSize () {

        //Update Contribution from Presets
        sizeMultiplier_presetAggregate = 1F;
        for (int i = 0; i < activePresets.Count; i++) {
            sizeMultiplier_presetAggregate *= (1F + activePresets[i].reference.sizeCurve.Evaluate(Time.time - activePresets[i].timeOfAddition));
        }

        //Set Physical Size
        transform.localScale *= baseSize * sizeMultiplier_presetAggregate / Mathf.Clamp(transform.localScale.magnitude, 0.01F, Mathf.Infinity);

    }

    public void UpdateColor () {

        //Reset Color Aggregate
        colorProcessing_aggregateSoFar.r = 1F;
        colorProcessing_aggregateSoFar.g = 1F;
        colorProcessing_aggregateSoFar.b = 1F;
        colorProcessing_aggregateSoFar.a = 1F;

        //Reset Weighting Variables
        colorProcessing_weightSoFar = 0.001F;
        colorProcessing_highestOpacitySoFar = 0F;

        for (int i = 0; i < activePresets.Count; i++) {
            colorProcessing_gradientAge = Time.time - activePresets[i].timeOfAddition + activePresets[i].gradientStartTimeRand;
            //Get Color from the Current Preset at the Current Time
            if (activePresets[i].reference.gradientLoops) {
                colorProcessing_curPresetColor = activePresets[i].reference.gradient.Evaluate(((colorProcessing_gradientAge) % activePresets[i].reference.gradientTime) / activePresets[i].reference.gradientTime);
            }
            else {
                colorProcessing_curPresetColor = activePresets[i].reference.gradient.Evaluate((colorProcessing_gradientAge) / activePresets[i].reference.gradientTime);
            }

            //Adjust Weighting Variables
            colorProcessing_weightSoFar += colorProcessing_curPresetColor.a;
            if (colorProcessing_curPresetColor.a > colorProcessing_highestOpacitySoFar) { colorProcessing_highestOpacitySoFar = colorProcessing_curPresetColor.a; }

            //Adjust Color Aggregate
            colorProcessing_aggregateSoFar = Color.Lerp(colorProcessing_aggregateSoFar, colorProcessing_curPresetColor, colorProcessing_curPresetColor.a / colorProcessing_weightSoFar);

        }

        //Recolor Bullet using Color Aggregate
        colorProcessing_aggregateSoFar.a = 1F;
        colorProcessing_colorToApply = Color.Lerp(baseColor, colorProcessing_aggregateSoFar, Mathf.Clamp01(colorProcessing_highestOpacitySoFar));
        sprite.color = colorProcessing_colorToApply;

    }

    protected void CheckOneShotRemoval () {

        presetsToRemove.Clear();

        for (int i = 0; i < activePresets.Count; i++) {
            if (activePresets[i].reference.oneShot) {
                if (Time.time - activePresets[i].timeOfAddition > 2F) {
                    presetsToRemove.Add(activePresets[i]);
                }
            }
        }

        for (int i = 0; i < presetsToRemove.Count; i++) {
            activePresets.Remove(presetsToRemove[i]);
        }

    }

    #endregion
    #region Public Functions (Add Preset, Remove Preset)

    public void AddPreset (BulletAppearancePreset newPreset) {

        //This kept presets unique per bullet. I'm not sure this is a good idea, since what if we want to add a really BIG red flash or something?
        //for (int i = 0; i < activePresets.Count; i++) {
        //    if (appearanceType == activePresets[i].reference.presetType) {
        //        activePresets[i].timeOfAddition = Time.time;
        //        return;
        //    }
        //}

        activePresets.Add(new BulletAppearance(newPreset));

    }

    public void MultiplyBaseSize (float multiplier) {

        baseSize *= multiplier;

    }

    #endregion
    #region Wiping

    public void WipeBulletAppearance () {

        //Remove all Active Presets from List
        activePresets.Clear();

        //TODO: Detach Particles. Tell them to re-attach once they're done playing out.

    }

    #endregion

}

public class BulletAppearance {

    public BulletAppearancePreset reference;
    public float timeOfAddition;
    public float gradientStartTimeRand;

    public BulletAppearance (BulletAppearancePreset newPresetReference) {
        reference = newPresetReference;
        timeOfAddition = Time.time;
        if (reference.randomGradientStartTime) {
            gradientStartTimeRand = Random.Range(0F, reference.gradientTime);
        }
    }

}