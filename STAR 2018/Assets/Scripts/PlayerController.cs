﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class PlayerController : MonoBehaviour {

    [HideInInspector] public EntityData entityData;     //We'll use [HideInInspector] to keep the Inspector view clean, but keep it "public" so we can access it from other scripts.

    private Vector2 moveVector = Vector2.zero;
    private Vector2 attackVector = Vector2.zero;
    private Vector2 lastAttackVector = Vector2.zero;

    private int selectedWeapon = 0;

    public GameObject aimLine;
    public Animator aimLineAnimator;
    private bool aimLineWasActiveLastFrame = false;

    void Update () {                                    //Update is called on every frame.

        //MOVEMENT
        moveVector.x = entityData.playerData.player.GetAxis("MoveX");
        moveVector.y = entityData.playerData.player.GetAxis("MoveY");

        transform.Translate(moveVector * entityData.EDT * entityData.moveSpeed * entityData.buffController.moveSpeed);

        //SHOOTING
        attackVector.x = entityData.playerData.player.GetAxis("AttackX");
        attackVector.y = entityData.playerData.player.GetAxis("AttackY");

        if (attackVector.magnitude > 0.25F) {
            lastAttackVector = attackVector;

            //Control Aimline
            aimLine.transform.rotation = Quaternion.identity;
            aimLine.transform.Rotate(0f, 0f, -90f - SuperFunctions.GameMath.degreesFromVector(lastAttackVector));
            if (!aimLineWasActiveLastFrame) aimLineAnimator.Play("Fade In");
            aimLineWasActiveLastFrame = true;
        }
        else {
            if (aimLineWasActiveLastFrame) aimLineAnimator.Play("Fade Out");
            aimLineWasActiveLastFrame = false;
        }

        if (entityData.playerData.player.GetButton("Shoot")) {
            entityData.guns[selectedWeapon].Shoot(lastAttackVector);
        }


        if (entityData.playerData.player.GetButton("MouseAttack")) {
            Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePosition.z = 0;
            entityData.guns[selectedWeapon].Shoot(mousePosition - transform.position);
        }

        //WEAPON SWITCHING
        if (entityData.playerData.player.GetButtonDown("SwitchWeapon1")) SwitchGun(0);
        if (entityData.playerData.player.GetButtonDown("SwitchWeapon2")) SwitchGun(1);
        if (entityData.playerData.player.GetButtonDown("SwitchWeapon3")) SwitchGun(2);
        if (entityData.playerData.player.GetButtonDown("SwitchWeapon4")) SwitchGun(3);

        //POWERS
        if (entityData.playerData.player.GetButtonDown("Power0")) TryPower(0);
        if (entityData.playerData.player.GetButtonDown("Power1")) TryPower(1);
        if (entityData.playerData.player.GetButtonDown("Power2")) TryPower(2);

    }

    //During setup, we'll set only the first gun to be active. The rest will be inactive until the player switches to that gun.
    public void SetUpGuns (EntityData entityData) {
        for (int i = 0; i < entityData.guns.Count; i++) {
            Debug.Log("Setting up gun " + i);
            entityData.guns[i].gameObject.SetActive(i == 0);
        }
    }

    void SwitchToNextGun () {
        selectedWeapon++;
        if (selectedWeapon >= entityData.guns.Count) selectedWeapon = 0;    //If we've reached the end of the guns on this player, wrap around to the first gun.
        for (int i = 0; i < entityData.guns.Count; i++) {
            entityData.guns[i].gameObject.SetActive(i == selectedWeapon);
        }
        if (entityData.guns[selectedWeapon].switchFX != null)
            entityData.guns[selectedWeapon].switchFX.PlayAllInstant();
    }

    void SwitchGun (int gunNum) {
        if (entityData.guns.Count > gunNum) {
            selectedWeapon = gunNum;
            for (int i = 0; i < entityData.guns.Count; i++) {
                entityData.guns[i].gameObject.SetActive(i == selectedWeapon);
            }
            if (entityData.guns[selectedWeapon].switchFX != null)
                entityData.guns[selectedWeapon].switchFX.PlayAllInstant();
        }
    }

    void TryPower (int powerNum) {
        if (entityData.powers.Count > powerNum) {
            entityData.powers[powerNum].TryActivate();
        }
    }

    public void EquipNewGun (GameObject gunPrefab) {
        GameObject newGun = Instantiate(gunPrefab, transform);
        newGun.GetComponent<Gun>().entityData = entityData;
        if (entityData.guns.Count < 4) {
            entityData.guns.Add(newGun.GetComponent<Gun>());
        } else {
            entityData.guns[entityData.nextGunEquipSlot] = newGun.GetComponent<Gun>();
        }
        newGun.SetActive(false);
        SwitchGun(entityData.nextGunEquipSlot);
        entityData.nextGunEquipSlot = (entityData.nextGunEquipSlot + 1) % 4;
    }

    public void EquipNewPower (GameObject powerPrefab) {
        GameObject newPower = Instantiate(powerPrefab, transform);
        newPower.GetComponent<Power>().entityData = entityData;
        if (entityData.powers.Count < 4) {
            entityData.powers.Add(newPower.GetComponent<Power>());
        }
        else {
            entityData.powers[entityData.nextPowerEquipSlot] = newPower.GetComponent<Power>();
        }
        entityData.nextPowerEquipSlot = (entityData.nextPowerEquipSlot + 1) % 4;
    }

}
