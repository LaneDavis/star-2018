﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Rewired;

    /*Welcome to EntityData!

    After looking through this script, you might think... this doesnt DO anything!
    You're absolutely right! This script just holds useful data (HP, vital stats) and references (like controller scripts).
    We'll use this script to easily allow each part of an entity to access any other part of that entity.
    */

public class EntityData : MonoBehaviour {
    
    //Parts of an Entity
    [HideInInspector] public List<Gun> guns = new List<Gun>();
    [HideInInspector] public List<Power> powers = new List<Power>();
    [HideInInspector] public List<EntityBody> bodies = new List<EntityBody>();
    [HideInInspector] public DamageTakenProcessor damageTakenProcessor;
    [HideInInspector] public DeathController deathController;
    [HideInInspector] public BuffController buffController;
    [HideInInspector] public EntityHealthIndicator healthIndicator;

    [Header("Base Stats")]

    //Health
    public float hpMax = 100F;
    public float hpCur = 100F;

    public float moveSpeed = 10F;

    //Timescale. Every time-related thing an entity does is multiplied by this value. Makes time-related buffs work.
    public float timescale = 1F;
    [HideInInspector] public float EDT = 0F;                          //Stands for Entity Delta Time. The length of the current frame multiplied by timescale.
    private const float tickLength = 0.25F;
    private float tickTimer = 0F;

    //Player Only
    public PlayerData playerData = null;
    public int playerNumber {
        get { return playerData == null ? -1 : playerData.playerNumber; }
    }
    [HideInInspector] public int nextGunEquipSlot = 0;
    [HideInInspector] public int nextPowerEquipSlot = 0;

    //Parts of a Player
    [HideInInspector] public PlayerController playerController;
    [HideInInspector] public RumbleController rumbleController;

    //Enemy Only
    [HideInInspector] public EnemyController enemyController;

    [Header("Rewards")]
    public int goldReward = 0;

    public virtual void Start () {                  //Start is called BEFORE the first frame. Use it to set things up!

        //Setup Damage Taken Processor.
        //NOTE: In RPG Games, a *ton* of different things happen when a character takes damage.
        //This processor gives us a single place to say "you take damage now." Other parts of a character can "subscribe"
        //to the DamageTakenProcessor and react properly (like by shaking or losing HP) when the character gets hurt.
        damageTakenProcessor = GetComponent<DamageTakenProcessor>();
        damageTakenProcessor.entityData = this;
        damageTakenProcessor.OnDeliverFinalDamage += DeductHP;

        deathController = GetComponent<DeathController>();
        deathController.entityData = this;

        foreach (EntityBody body in GetComponentsInChildren<EntityBody>()) {
            body.entityData = this;
            body.damageTakenProcessor = damageTakenProcessor;
            body.Setup();
            bodies.Add(body);
        }
        
        foreach (Gun gun in GetComponentsInChildren<Gun>()) {
            if (gun.transform.parent.GetComponent<Power>() == null) { //We don't want to add guns from powers into this list.
                gun.entityData = this;
                guns.Add(gun);
            }
        }
        if (GetComponent<PlayerController>() != null) {
            GetComponent<PlayerController>().SetUpGuns(this);
        }
        nextGunEquipSlot = guns.Count % 4;


        foreach (Power power in GetComponentsInChildren<Power>()) {
            power.entityData = this;
            powers.Add(power);
        }
        nextPowerEquipSlot = powers.Count % 4;

        if (GetComponent<EnemyController>() != null) {
            SetupEnemy();
        }

        if (GetComponentInChildren<EntityHealthIndicator>() != null) {
            healthIndicator = GetComponentInChildren<EntityHealthIndicator>();
        }

        if(GetComponent<BuffController>() != null) {
            buffController = GetComponent<BuffController>();
            damageTakenProcessor.OnDeliverFinalDamage += buffController.ApplyBuffsFromDamageSource;
            buffController.EntityData = this;
        }

    }

    public void SetupPlayer (PlayerData newPlayerData) {

        playerData = newPlayerData;

        playerData.player = ReInput.players.GetPlayer(playerData.playerNumber);   //We'll set a reference to the player's controller so other scripts can use it.

        //The next few lines identify parts of the player so we can easily access them later.
        playerController = GetComponent<PlayerController>();
        playerController.entityData = this;
        rumbleController = GetComponent<RumbleController>();
        rumbleController.entityData = this;

    }

    public void SetupEnemy () {
        enemyController = GetComponent<EnemyController>();
        enemyController.entityData = this;
        EnemyManager.instance.AddEnemy(this);
    }

    public virtual void Update () {

        EDT = Time.deltaTime * timescale;
        tickTimer += EDT;
        if (tickTimer > tickLength) {
            tickTimer -= tickLength;
            Tick();
        }

    }

    public virtual void Tick () {

        foreach (DamageInstance instance in buffController.GetDamageOverTimeInstances()) {
            damageTakenProcessor.ProcessDamage(instance);
        }

    }

    public void AddHP (float amount) {

        amount = Mathf.Clamp(amount, 0f, Mathf.Infinity);
        hpCur = Mathf.Clamp(hpCur + amount, 0f, hpMax);
        if (healthIndicator != null) healthIndicator.SetHealthPercent(hpCur / hpMax);
        
    }

    public void DeductHP (DamageInstance damageInstance) {
        hpCur -= damageInstance.amount * buffController.dmgTaken;
        if (healthIndicator != null) healthIndicator.SetHealthPercent(hpCur / hpMax);
        if (hpCur <= 0F) deathController.Die();
    }

    /// <summary>
    /// Is this entity subject to damage from the incoming source? If they're allied, return no so we can ignore that damage.
    /// </summary>
    /// <param name="otherPlayerNumber">The player number of the source of damage. -1 for non-players.</param>
    /// <returns>True if this entity is enemies with the source of incoming damage. False if they're allies.</returns>
    public bool IsHurtByPlayer (int otherPlayerNumber) {

        //If the damage is from an Enemy NPC Character
        if (otherPlayerNumber == -1) {
            if (playerData == null) return false;       //False because this is an Enemy NPC too.
            if (playerData.playerAlliances.isFriendlyWithAIs) return false;
            return true;
        }

        //If the damage is from a player
        else {
            if (playerData == null) {               //If this is an Enemy NPC...
                if (PlayerManager.instance.PlayerDataFromPlayerNumber(otherPlayerNumber).playerAlliances.isFriendlyWithAIs) return false;   //False because that player is on the Enemy NPC's Side.
                return true;                        //Otherwise true.
            }
            else {
                if (playerData.playerAlliances.isFriendlyWithPlayer(otherPlayerNumber)) return false;
                return true;
            }
        }

    }

    public bool IsAnEnemy () {
        return enemyController != null;
    }

}
