﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Rewired;

public class PlayerManager : MonoBehaviour {

    public static PlayerManager instance;
    public bool PlayerVersusPlayer = false;

    public List<PlayerData> playerDatas = new List<PlayerData>();
    public int maxPlayers = 5;

    public GameObject playerPrefab;

    public List<Color> playerColors;

    public List<CharacterClass> characterClasses;
    public List<ClassSelectionController> selectionControllers;

    void Awake () {
        instance = this;
    }

    void Update () {

        CheckForNewPlayers();

    }

    void CheckForNewPlayers () {

        for (int i = 0; i < maxPlayers; i++) {
            if (ReInput.players.GetPlayer(i).GetButtonDown("PauseJoin")) {
                if (!PlayerNumberIsActive(i)) {
                    ActivateClassSelector(i, ReInput.players.GetPlayer(i));
                }
            }
        }

    }

    /// <summary>
    /// Checks whether the chosen playerNumber is being used by any active player.
    /// Helps ensure each newly-spawned character is controlled by a different player.
    /// </summary>
    /// <param name="playerNumber">The player number we're checking.</param>
    /// <returns>True if the player number is in use by an active player. False otherwise.</returns>
    public bool PlayerNumberIsActive (int playerNumber) {
        if (PlayerDataFromPlayerNumber(playerNumber) == null) return false;
        return true;
    }

    public PlayerData PlayerDataFromPlayerNumber (int playerNumber) {
        foreach (PlayerData pd in playerDatas) {
            if (pd.playerNumber == playerNumber) return pd;
        }
        return null;
    }

    private void ActivateClassSelector (int playerNumber, Player player) {

        //Setup the new player's PlayerData
        PlayerData newPlayerData = new PlayerData(playerNumber);
        playerDatas.Add(newPlayerData);
        selectionControllers[playerNumber].Setup(playerNumber, player, characterClasses);

    }

    /// <summary>
    /// Spawns a new player character and assigns it to the player who just pressed Start.
    /// </summary>
    /// <param name="playerNumber">The number assigned to the player.</param>
    public void SpawnPlayer (int playerNumber, CharacterClass characterClass) {

        PlayerData newPlayerData = (PlayerDataFromPlayerNumber(playerNumber));

        GameObject newCharacter = GameObject.Instantiate(playerPrefab, new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, 0f), Quaternion.identity, null);

        EntityData newEntityData = newCharacter.GetComponent<EntityData>();
        newEntityData.SetupPlayer(newPlayerData);
        newPlayerData.entityData = newEntityData;
        newPlayerData.playerObject = newCharacter;
        newPlayerData.playerColor = playerColors[playerNumber];
        foreach (SpriteRenderer sprite in newCharacter.GetComponentsInChildren<SpriteRenderer>()) {
            if (sprite.name != "Health Indicator")
                sprite.color = newPlayerData.playerColor;
        }

        //Set up the character class.
        newEntityData.hpMax = characterClass.maxHP;
        newEntityData.hpCur = characterClass.maxHP;
        newEntityData.moveSpeed = characterClass.moveSpeed;
        newCharacter.transform.localScale *= characterClass.size;
        foreach (GameObject gun in characterClass.gunPrefabs) {
            GameObject newGun = Instantiate(gun, newCharacter.transform.position, Quaternion.identity, newCharacter.transform);
        }
        foreach (GameObject power in characterClass.powerPrefabs) {
            GameObject newPower = Instantiate(power, newCharacter.transform.position, Quaternion.identity, newCharacter.transform);
        }

    }

    public List<PlayerData> ActivePlayers () {
        List<PlayerData> list = new List<PlayerData>();
        foreach (PlayerData pd in playerDatas) { 
            if (pd.playerObject != null)
                list.Add(pd);
        }
        return list;
    }

    public PlayerData NearestPlayerToPoint (Vector2 referencePoint) {
        PlayerData bestPDSoFar = null;
        float bestDistSoFar = Mathf.Infinity;
        float dist = 0F;
        foreach (PlayerData pd in playerDatas) {
            if (pd.playerObject != null) {
                dist = Vector2.Distance(referencePoint, pd.playerObject.transform.position);
                if (dist < bestDistSoFar) {
                    bestDistSoFar = dist;
                    bestPDSoFar = pd;
                }
            }
        }
        return bestPDSoFar;
    }

}

public class PlayerAlliances {

    public List<bool> alliances = new List<bool>();
    public bool isFriendlyWithAIs = false;

    public PlayerAlliances (int playerNumber) {
        for (int i = 0; i < PlayerManager.instance.maxPlayers; i++) {
            if (i == playerNumber) alliances.Add(true);
            else alliances.Add(!PlayerManager.instance.PlayerVersusPlayer);
        }
    }

    public bool isFriendlyWithPlayer (int playerNumber) {
        if (alliances.Count <= playerNumber) return true;
        if (playerNumber < 0) return true;
        return alliances[playerNumber];
    }

}