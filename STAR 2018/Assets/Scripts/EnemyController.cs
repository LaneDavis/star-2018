﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    [HideInInspector] public EntityData entityData;
    [HideInInspector] public GameObject targetPlayer;    //Standard enemies base their movement/shooting on the nearest player.

}

    /*For now, this script just exists so that all possible types of Enemy Controllers can be referenced easily by EntityData.

    Since BasicEnemyController inherits from EnemyController, EntityData can set its reference to entityData here. This will
    effortlessly expand to other types of EnemyController, letting fancier enemies get set up in the same way.
    */
