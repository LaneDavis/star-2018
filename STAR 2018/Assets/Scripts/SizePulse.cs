﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SuperFunctions;

public class SizePulse : MonoBehaviour {

    //STATE
    protected bool isActive = false;

    [Header("Size")]
    public AnimationCurve sizeCurve = new AnimationCurve(new Keyframe(0f, 0.5f), new Keyframe(0.5f, -0.5f), new Keyframe(1f, 0.5f));
    public AnimationCurve sizeEaseInCurve = new AnimationCurve(new Keyframe(0F, 0F, 2f, 2f), new Keyframe(0.5f, 1f));
    [Range(0F, 10F)]
    public float sizeSpeed = 1f;
    [Range(0F, 10F)]
    public float sizeDecayRate = 5f;
    private List<SizeFactor> sizeFactors = new List<SizeFactor>();
    private float baseSize;

    [Header("Editor Controls")]
    public bool ed_addSize = false;

    void ActivationSetup () {

        //Set Base Position and Size
        baseSize = transform.localScale.magnitude;

    }

    void Update () {

        if (isActive) {
            UpdateSize();
            DeleteOldSizeFactors();
        }

        //Apply Editor Controls
        if (ed_addSize) {
            ed_addSize = false;
            if (!isActive) { Activate(); }
            AddSizeFactor(1f);
        }

    }

    #region Update Functions

    void UpdateSize () {

        float newSize = baseSize;

        //For Each Size Factor...
        for (int i = 0; i < sizeFactors.Count; i++) {

            //Update Age of Factor
            sizeFactors[i].age += Time.deltaTime;

            //Decay Factor Strength
            sizeFactors[i].strength = GameMath.DecayFloat(sizeFactors[i].strength, sizeDecayRate, Time.deltaTime);

            //Apply Factor to 'newSize'
            newSize *= (1f + (sizeCurve.Evaluate(sizeFactors[i].age * sizeSpeed) * sizeFactors[i].strength * sizeEaseInCurve.Evaluate(sizeFactors[i].age)));

        }

        //Apply New Size to Body
        transform.localScale *= newSize / transform.localScale.magnitude;

    }

    void DeleteOldSizeFactors () {

        List<SizeFactor> sizeFactorsToDelete = new List<SizeFactor>();

        //Determine which Size Factors are so weak that they're invisible.
        for (int i = 0; i < sizeFactors.Count; i++) {

            if (sizeFactors[i].age > 2F) {
                sizeFactorsToDelete.Add(sizeFactors[i]);
            }

        }

        //Remove Old Size Factors from Tracking
        for (int i = 0; i < sizeFactorsToDelete.Count; i++) {

            sizeFactors.Remove(sizeFactorsToDelete[i]);

        }

    }

    #endregion
    #region Public Command Functions

    public void Activate () {

        if (!isActive) {
            ActivationSetup();
            isActive = true;
        }

    }

    public void AddSizeFactor (float strength) {

        if (!isActive) {
            Activate();
        }
        sizeFactors.Add(new SizeFactor(strength));

    }

    #endregion

}

public class SizeFactor {

    public float age;
    public float strength;

    public SizeFactor (float newStrength) {

        strength = newStrength;

    }

}

