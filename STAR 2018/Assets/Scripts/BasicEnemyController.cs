﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicEnemyController : EnemyController {

          
    protected float tickLength = 0.25F;     //There might be a LOT of enemies. We don't want them all doing everything every frame. This is one way to ease back on processing.
    protected float tickTimer = 0F;

    public float awakeDistance = 15F;       //Enemies won't do anything if they don't have a target within this distance.
    public float sleepDistance = 25F;       //Once an enemy has acquired you as a target, you must go past this distance to escape.

    [Header("Movement")]
    [Range(0F, 20F)] public float desiredDistance = 5F;
    protected Vector2 desiredPosition = Vector2.zero;

    [Header("Attack")]
    [Range(0F, 10F)] public float shootCooldown = 2F;   //We could also just use the Enemy's gun cooldown, but we'll specify a cooldown here so we can give it some pre-shoot behaviors like slowing and pulsing.
    [Range(0F, 1F)] public float shootWindup = 0.6F;
    public BuffPreset windupSlowdownBuff;
    public FX_Controller windupInstantFX;
    public FX_Controller windupFX;
    public FX_Controller shootFX;
    protected ActionSequencer shootSequence;            //We'll use a custom tool called ActionSequencer to make it easy to schedule the windup and shoot parts of the attack.
    private float shootTimer = 0F;

    [Header("Avoidance")]
    public AnimationCurve avoidanceCurve = new AnimationCurve(new Keyframe(0F, 0.5F, -0.25F, -0.25F), new Keyframe(3F, 0F, 0F, 0F));

    private bool firstUpdate = true;

    protected virtual void Start () {
        shootSequence = new ActionSequencer();
        shootSequence.AddNextStep(StartShootWindup, 0F, this);
        shootSequence.AddNextStep(StopWindupAndShoot, shootWindup, this);
        shootSequence.AddEndStep();
    }

    protected virtual void FirstUpdate () {
        firstUpdate = false;
        entityData.damageTakenProcessor.OnDeliverFinalDamage += AggroNearbyEnemies;
    }

    protected virtual void Update () {  //This function is "virtual" which means it can be overridden in child scripts. To see this in action, check out the NecromancerController script

        if (firstUpdate) {  //This happens after Start(), so it's guaranteed that the EntityData is fully set up.
            FirstUpdate();
        }

        //Update Tick (performs occasional actions)
        tickTimer += entityData.EDT;
        if (tickTimer > tickLength) Tick();

        if (targetPlayer != null) {       //If all players are dead or too far away, we won't do anything else after this point.
            Move();
            UpdateShooting();
        }

	}

    void Tick () {
        tickTimer = 0F;
        AcquireTarget();
    }

    void AcquireTarget () {
        PlayerData nearestPlayer = PlayerManager.instance.NearestPlayerToPoint(transform.position);
        if (nearestPlayer != null) {
            if (Vector2.Distance(transform.position, nearestPlayer.playerObject.transform.position) > awakeDistance)
                if (targetPlayer == null)
                    return;
                else {
                    if (Vector2.Distance(transform.position, nearestPlayer.playerObject.transform.position) > sleepDistance) {
                        targetPlayer = null;
                    }
                }
            else
                targetPlayer = nearestPlayer.playerObject;
        }
    }

    void Move () {

        //Set Desired Position
        Vector2 playerPos = targetPlayer.transform.position;
        Vector2 dirFromPlayer = ((Vector2)transform.position - playerPos).normalized;
        desiredPosition = playerPos + dirFromPlayer * desiredDistance;

        //Move Toward Desired Position
        Vector2 vecToDesiredPos = desiredPosition - (Vector2)transform.position;
        transform.Translate(vecToDesiredPos.normalized * Mathf.Clamp01(vecToDesiredPos.magnitude) * entityData.EDT * entityData.moveSpeed * entityData.buffController.moveSpeed);

        //Get Nudged Away from Other Enemies
        foreach (EntityData enemy in EnemyManager.instance.activeEnemies) {
            if (enemy == entityData) continue;
            Vector2 v = transform.position - enemy.gameObject.transform.position;
            transform.Translate(v.normalized * avoidanceCurve.Evaluate(v.magnitude));
        }

    }

    protected void UpdateShooting () {

        if (shootSequence.isActive) {
            shootSequence.UpdateTime(entityData.EDT * entityData.buffController.fireRate);
        }

        else {
            shootTimer += entityData.EDT * entityData.buffController.fireRate;
            if (shootTimer > shootCooldown) {
                shootTimer = 0F;
                shootSequence.isActive = true;
            }
        }

    }

    void StartShootWindup (object obj = null) {
        if (windupSlowdownBuff != null)
            entityData.buffController.AddBuff(windupSlowdownBuff);
        if (windupInstantFX != null)
            windupInstantFX.PlayAllInstant();
        if (windupFX != null)
            windupFX.ToggleAll(true);
    }

    void StopWindupAndShoot (object obj = null) {

        if (windupFX != null)
            windupFX.ToggleAll(false);

        if (targetPlayer != null) {       //If all players are dead or too far away, we won't do anything else after this point.

            if (shootFX != null)
                shootFX.PlayAllInstant();

            foreach (Gun gun in entityData.guns) {
                gun.Shoot(((Vector2)targetPlayer.transform.position) - (Vector2)transform.position);
            }

        }

    }

    private void AggroNearbyEnemies (DamageInstance damageInstance) {
        if (damageInstance.ownerObject != null) {
            EnemyManager.instance.AggroEnemiesInRadius(transform.position, 7f, damageInstance.ownerObject);
        }
    }

}
