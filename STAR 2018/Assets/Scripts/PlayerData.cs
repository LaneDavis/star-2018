﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Rewired;
    
public class PlayerData {
    
    public int playerNumber = -1;
    public Color playerColor;
    public Player player;                           //This is a reference to the player's gamepad. We'll use it to determine what the player is doing.
    public PlayerAlliances playerAlliances;         //A reference to which players this player is friendly to. If you're unfriendly to that player, your attacks hurt them!
    public GameObject playerObject;                 //The character GameObject controlled by this player.
    public EntityData entityData;                   //The script with all the data about the player object.

    public PlayerData (int playerNumber) {
        this.playerNumber = playerNumber;
        player = ReInput.players.GetPlayer(playerNumber);
        playerAlliances = new PlayerAlliances(playerNumber);
    }

}

    /*
    PlayerData is for storing player information that persists between lives.

    Since players will be able to die or change character, we'll want some way to record permanent
    information like that player's controller slot and who that player is allied with in competitive games.
    */
