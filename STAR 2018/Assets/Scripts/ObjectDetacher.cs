﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDetacher : MonoBehaviour {

    private float timeOfDeath = Mathf.Infinity;

    void Update () {
        if (Time.time > timeOfDeath)
            Destroy(gameObject);
    }

    public void Detach () {
        transform.SetParent(null);
    }

    public void DetachAndScheduleDeath (float secondsUntilDeath) {
        timeOfDeath = Time.time + secondsUntilDeath;
        Detach();
    }

}

    /*
    ObjectDeatcher detaches objects! It then schedules them for deletion and later kills them.

    But why? Can't we just Destroy(objectName) and then everything attached to objectName will be destroyed along with it?

    We can, but this produces some effects we don't want! When an object is destroyed, all particles and sounds on that object
    are deleted INSTANTLY. This means that instead of a sound or particle fading away nicely, they just all pop out of existence
    all at once. Very unsatisfying.

    For this reason, when an entity or bullet dies, it will look for all child objects that contain ParticleSystems and slap an
    ObjectDetacher on them. It will then call ObjectDetacher.DetachAndScheduleDeath with a time of 10 seconds, which is long
    enough for all the death particle systems to fully play out.
    */
