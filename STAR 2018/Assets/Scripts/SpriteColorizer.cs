﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpriteColorizer : MonoBehaviour {

    //ID
    public List<SpriteRenderer> spritesWatched = new List<SpriteRenderer>();

    //ENTITY - The macro-scale being that owns this component
    protected GameObject entity;
    [HideInInspector]
    public GameObject Entity { get { return entity; } set { entity = value; } }

    //Default Values
    public List<float> defR = new List<float>();
    public List<float> defG = new List<float>();
    public List<float> defB = new List<float>();
    public List<float> defA = new List<float>();

    //Current Values
    protected float curR;
    protected float curG;
    protected float curB;
    protected float curA;

    //COLOR FACTORS
    public List<ColorFactor> colorFactors = new List<ColorFactor>();
    public float defaultColorStrength = 1F;
    protected float defaultColorPower = 0F;

    //FLICKER
    float flickerPulseTime = 0.1F;
    float nextFlickerTime;
    float flickerStopTime;
    bool isCurrentlyFlickering;

    //DELETION
    bool isScheduledForDeletion = false;
    float deletionTime = 0F;
    float defaultDeleteDuration = 0.8F;
    public bool detachesOnDeath = true;

    public bool affectsAlpha = true;

    //FORCE
    [Range(0F, 10F)]
    public float forceMultiplier = 1F;

    virtual public void Start () {

        spritesWatched.Add(GetComponent<SpriteRenderer>());

        for (int i = 0; i < spritesWatched.Count; i++) {
            defR.Add(spritesWatched[i].color.r);
            defG.Add(spritesWatched[i].color.g);
            defB.Add(spritesWatched[i].color.b);
            defA.Add(spritesWatched[i].color.a);
        }
        curR = defR[0];
        curG = defG[0];
        curB = defB[0];
        curA = defA[0];

    }

    public virtual void Update () {

        //FLICKER
        UpdateFlicker();

        //COLOR
        FadeFactors();
        UpdateCurColors();
        CheckForFactorDeletion();

        //APPLICATION
        ApplyCurColor();

        //DELETION
        CheckForSelfDeletion();

    }

    public void AddColorFactor (Color newColor, float duration, float startStrength, string newFactorName = "") {
        if (newFactorName == "") {
            colorFactors.Add(new ColorFactor(newColor, duration, startStrength, newFactorName));
        }
        else {
            bool thatNameHasBeenUsed = false;
            for (int i = 0; i < colorFactors.Count; i++) {
                if (newFactorName == colorFactors[i].name) {
                    colorFactors[i].UpdateFactor(newColor, duration, startStrength);
                    thatNameHasBeenUsed = true;
                    break;
                }
            }
            if (!thatNameHasBeenUsed) {
                colorFactors.Add(new ColorFactor(newColor, duration, startStrength, newFactorName));
            }
        }
    }

    public void AddColorFactor (Color newColor, float duration, float startStrength, string newFactorName, bool fadesBool) {
        bool thatNameHasBeenUsed = false;
        for (int i = 0; i < colorFactors.Count; i++) {
            if (newFactorName == colorFactors[i].name) {
                colorFactors[i].UpdateFactor(newColor, duration, startStrength);
                thatNameHasBeenUsed = true;
                break;
            }
        }
        if (!thatNameHasBeenUsed) {
            colorFactors.Add(new ColorFactor(newColor, duration, startStrength, newFactorName, fadesBool));
        }
    }

    protected void ApplyCurColor () {
        float added = SumFactorStrength();
        float total = added + defaultColorStrength;

        for (int i = 0; i < spritesWatched.Count; i++) {
            if (affectsAlpha) { spritesWatched[i].color = new Color((curR * added + defR[i] * defaultColorStrength) / total, (curG * added + defG[i] * defaultColorStrength) / total, (curB * added + defB[i] * defaultColorStrength) / total, curA); }
            else { spritesWatched[i].color = new Color((curR * added + defR[i] * defaultColorStrength) / total, (curG * added + defG[i] * defaultColorStrength) / total, (curB * added + defB[i] * defaultColorStrength) / total, spritesWatched[i].color.a); }
        }
    }

    protected void UpdateCurColors () {
        float sumFactorStrength = SumFactorStrength();
        if (sumFactorStrength <= 0) {
            curR = 0F; curG = 0F; curB = 0F;
        }
        else {
            curR = SumReds() / sumFactorStrength;
            curG = SumGreens() / sumFactorStrength;
            curB = SumBlues() / sumFactorStrength;
        }
    }

    protected void FadeFactors () {
        for (int i = 0; i < colorFactors.Count; i++) {
            if (colorFactors[i].fades) {
                colorFactors[i].strength *= (colorFactors[i].endTime - Time.time) / (colorFactors[i].endTime - Time.time + Time.deltaTime);
            }
        }
    }

    protected void CheckForFactorDeletion () {
        for (int i = colorFactors.Count - 1; i >= 0; i--) {
            if (Time.time > colorFactors[i].endTime) {
                colorFactors.RemoveAt(i);
            }
        }
    }

    protected float SumReds () {
        float redSoFar = 0F;
        for (int i = 0; i < colorFactors.Count; i++) {
            redSoFar += colorFactors[i].color.r * colorFactors[i].strength;
        }
        return redSoFar;
    }

    protected float SumGreens () {
        float greenSoFar = 0F;
        for (int i = 0; i < colorFactors.Count; i++) {
            greenSoFar += colorFactors[i].color.g * colorFactors[i].strength;
        }
        return greenSoFar;
    }

    protected float SumBlues () {
        float blueSoFar = 0F;
        for (int i = 0; i < colorFactors.Count; i++) {
            blueSoFar += colorFactors[i].color.b * colorFactors[i].strength;
        }
        return blueSoFar;
    }

    protected float SumFactorStrength () {
        //float strengthSoFar = defaultColorStrength;	//Initialize to 1 to account for the default color that gets factored in.
        float strengthSoFar = 0F;
        for (int i = 0; i < colorFactors.Count; i++) {
            strengthSoFar += colorFactors[i].strength;
        }
        return strengthSoFar;
    }

    public void SetA (float aValue) {
        curA = aValue;
    }

    protected void UpdateFlicker () {
        if (isCurrentlyFlickering) {
            if (Time.time >= flickerStopTime) {
                StopFlicker();
            }
            else if (Time.time >= nextFlickerTime) {
                OneFlicker();
            }
        }
    }

    public void StartFlicker (float duration) {
        isCurrentlyFlickering = true;
        flickerStopTime = Time.time + duration;
    }

    protected void StopFlicker () {
        isCurrentlyFlickering = false;
        curA = defA[0];
    }

    protected void OneFlicker () {
        if (spritesWatched[0].color.a > defA[0] * 0.25F) {
            for (int i = 0; i < spritesWatched.Count; i++) {
                curA = defA[0] * 0.0F;
            }
        }
        else {
            curA = defA[0] * 1F;
        }
        nextFlickerTime = Time.time + flickerPulseTime;
    }

    public void Detach () {

        if (detachesOnDeath) {

            for (int i = 0; i < spritesWatched.Count; i++) {

                spritesWatched[i].transform.parent = null;
                StartFlicker(defaultDeleteDuration);
                deletionTime = Time.time + defaultDeleteDuration;
                isScheduledForDeletion = true;

                //Apply Force
                Rigidbody2D newBody = spritesWatched[i].gameObject.AddComponent<Rigidbody2D>() as Rigidbody2D;
                newBody.AddForce(new Vector2(Random.Range(-.3F, .5F), Random.Range(-.3F, .1F)) * 600F * forceMultiplier);
                newBody.AddTorque(Random.Range(-300F, 300F));

            }

        }
        else {
            Destroy(gameObject);
        }

    }

    protected void CheckForSelfDeletion () {
        if (isScheduledForDeletion && deletionTime != 0f && deletionTime < Time.time) {
            Destroy(gameObject);
        }
    }

}

[System.Serializable]
public class ColorFactor {

    public float endTime;
    public Color color;
    public float strength;
    public string name;
    public bool fades = false;

    public ColorFactor (Color newColor, float newDuration, float startStrength, string newFactorName, bool fadesBool) {
        endTime = Time.time + newDuration;
        color = newColor;
        strength = startStrength;
        name = newFactorName;
        fades = fadesBool;
    }

    public ColorFactor (Color newColor, float newDuration, float startStrength, string newFactorName) {
        endTime = Time.time + newDuration;
        color = newColor;
        strength = startStrength;
        name = newFactorName;
    }

    public void UpdateFactor (Color newColor, float newDuration, float newStrength) {
        color = newColor;
        endTime = Time.time + newDuration;
        strength = newStrength;
    }
}
