﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SuperFunctions {

    public class GameMath : MonoBehaviour {

        public static float DecayFloat (float inputFloat, float decayRate, float time) {
            return inputFloat * Mathf.Exp(-time * decayRate);
        }

        public static List<PlayerData> playersInRadius (Vector2 centerPoint, float radius) {
            List<PlayerData> list = PlayerManager.instance.ActivePlayers();
            for (int i = list.Count - 1; i >= 0; i--) {
                if (Vector2.Distance(centerPoint, list[i].playerObject.transform.position) > radius)
                    list.RemoveAt(i);
            }
            return list;
        }

        public static bool isClockwise (Vector2 checkedVector, Vector2 anchorVector) {
            return (-anchorVector.x * checkedVector.y + anchorVector.y * checkedVector.x > 0F);
        }

        public static float RotationToLookAtPoint (Vector2 lookDir, Vector2 vectorToPoint) {
            float s = 1F;
            if (isClockwise(vectorToPoint, lookDir)) s *= -1F;
            return (s * Vector2.Angle(vectorToPoint, lookDir));
        }

        public static Vector2 RotateVector (Vector2 v, float degrees) {
            float ca = Mathf.Cos(degrees * Mathf.Deg2Rad);
            float sa = Mathf.Sin(degrees * Mathf.Deg2Rad);
            return new Vector2(ca * v.x - sa * v.y, sa * v.x + ca * v.y);
        }

        public static float SoftClamp (float input, float min, float max, float decayRate, float time) {
            float i = input;
            float ci = Mathf.Clamp(input, min, max);
            float outOfbounds = i - ci;
            outOfbounds = DecayFloat(outOfbounds, decayRate, time);
            return ci + outOfbounds;
        }

        public static float degreesFromVector (Vector2 v) {
            Vector2 anchorVector = new Vector2(1, 0);
            float ang = Vector2.Angle(anchorVector, v);
            Vector3 cross = Vector3.Cross(anchorVector, v);
            if (cross.z > 0) { ang = 360F - ang; }
            return ang;
        }

        public static int ProbabilisticCeilOrFloor (float floatVal) {
            return Random.Range(0F, 1F) > floatVal % 1F ? (int)Mathf.Floor(floatVal) : (int)Mathf.Ceil(floatVal);
        }

        public static Color BlendWeightedColors (List<ColorWeightPair> colors) {
            float r = 0F; float g = 0F; float b = 0F; float a = 0F;
            float totalWeight = 0F;
            foreach (ColorWeightPair color in colors) {
                r += color.color.r * color.weight;
                g += color.color.g * color.weight;
                b += color.color.b * color.weight;
                a += color.color.a * color.weight;
                totalWeight += color.weight;
            }
            r /= totalWeight; g /= totalWeight; b /= totalWeight; a /= totalWeight;
            return new Color(r, g, b, a);
        }

        public static int RandomNumberWithExclusion (int min, int max, int exclusion) {
            if (max - min <= 1) {
                Debug.LogError("RandomNumberWithExclusion must have at least one returnable value.");
                return 0;
            }
            else {
                int r = Random.Range(min, max - 1);
                if (r >= exclusion) r++;
                return r;
            }
        }

    }

    public class ColorWeightPair {
        public Color color;
        public float weight;
        public ColorWeightPair (Color color, float weight) { this.color = color; this.weight = weight; }
    }

}


