﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXDropInstantFX : FX {
    
    public GameObject sfxPrefab;
    
    public float dropDelay = 0F;
    public bool durationDropOnEnd = false;

    protected bool isCounting = false;
    protected float timeElapsed = 0F;
    protected bool didDrop = false;

    protected GameObject anchorObject;

    void Update () {

        if (isCounting) timeElapsed += Time.deltaTime;
        if (timeElapsed > dropDelay && !didDrop) {
            Drop();
        }

    }

    public override void PlayInstant (GameObject anchorObject = null, float instantMultiplier = 1F) {

        isCounting = true;
        if (anchorObject != null) this.anchorObject = anchorObject;

    }

    protected override void ToggleGoesOn () {

        isCounting = true;

    }

    protected override void ToggleGoesOff () {

        isCounting = false;
        if (!didDrop && durationDropOnEnd) {
            Drop();
        }

    }

    void Drop () {

        didDrop = true;
        GameObject sfxObject = GameObject.Instantiate(sfxPrefab, transform.position, Quaternion.identity, null) as GameObject;
        if (anchorObject != null) sfxObject.GetComponent<FX_Controller>().PlayAllInstant(anchorObject);
        else sfxObject.GetComponent<FX_Controller>().PlayAllInstant(null);

    }

    public override void Reset () {
        base.Reset();
        isCounting = false;
        timeElapsed = 0F;
        didDrop = false;
    }

}
