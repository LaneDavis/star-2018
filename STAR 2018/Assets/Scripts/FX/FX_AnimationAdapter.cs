﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FX_AnimationAdapter : MonoBehaviour {

    public List<FX_Controller> sfxPackages = new List<FX_Controller>();

    public void PlayInstant (int packageNumber) {

        if (sfxPackages.Count > packageNumber) {
            sfxPackages[packageNumber].PlayAllInstant(gameObject);
        }

    }

    public void ToggleDurationOn (int packageNumber) {

        if (sfxPackages.Count > packageNumber) {
            sfxPackages[packageNumber].ToggleAll(true, gameObject);
        }

    }

    public void ToggleDurationOff (int packageNumber) {

        if (sfxPackages.Count > packageNumber) {
            sfxPackages[packageNumber].ToggleAll(false, gameObject);
        }

    }

}
