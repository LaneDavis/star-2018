﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using SuperFunctions;

public class FXSound : FX {

    public SoundCall instantSound;
    public SoundCall durationLoop;
    protected float baseDurationVolume = 0F;

    public AnimationCurve durationCurve = AnimationCurve.EaseInOut(0F, 0F, 1F, 1F);
    public float durationDecaySpeed = 5F;

    //State: Time
    protected float durationTimer;
    //State: Strength
    protected float curDurationMultiplier;

    void Start () {
        baseDurationVolume = durationLoop.volume;
        durationLoop.volume = 0F;
    }

    public override void PlayInstant (GameObject anchorObject = null, float instantMultiplier = 1F) {
        //TODO: Do we multiply the sound's volume here? I'm not sure yet.
        SoundManager.instance.PlaySound(instantSound, gameObject);
    }

    protected override void ToggleGoesOn () {
        durationCurve.keys[0].value = curDurationMultiplier;
        durationTimer = 0F;
    }

    protected override void ToggleGoesOff () {
        durationCurve.keys[0].value = 0F;
        durationTimer = 0F;
    }

    void Update () {

        if (toggleState) {
            durationTimer += Time.deltaTime * speedMultiplier;
            curDurationMultiplier = durationCurve.Evaluate(durationTimer);
        }
        else {
            curDurationMultiplier = SuperFunctions.GameMath.DecayFloat(curDurationMultiplier, durationDecaySpeed, Time.deltaTime * speedMultiplier);
        }

        durationLoop.volume = baseDurationVolume * curDurationMultiplier;
        if (curDurationMultiplier > 0.05F) {
            SoundManager.instance.PlaySound(durationLoop, gameObject);
        }

    }

    public override void Reset () {
        base.Reset();
        durationTimer = 0F;
        curDurationMultiplier = 1F;
    }

}

#if UNITY_EDITOR
[CustomEditor(typeof(FXSound))]
public class SFX_Sound_Editor : Editor {


    FXSound soundScript;

    public override void OnInspectorGUI () {
        base.OnInspectorGUI();
        soundScript = (FXSound)target;
        if (GUILayout.Button("Null Loaded Sounds")) {
            NullSound(soundScript.instantSound);
            NullSound(soundScript.durationLoop);
        }
    }

    void NullSound (SoundCall sound) {
        sound.soundKey = "";
        sound.soundUsed = null;
        sound.priority = 0;
        sound.volume = 0;
        sound.pitch = 0;
        sound.pitchVariation = 0;
        sound.autoFadeRate = 0;
        sound.autoFades = false;
    }
}
#endif