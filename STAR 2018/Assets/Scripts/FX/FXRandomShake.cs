﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXRandomShake : FX {

    protected GameObject anchorObject;
    protected List<RandomShake> targets = new List<RandomShake> ();
    
    public float instantShake;
    public AnimationCurve durationShake;
    public float durationTime = 1F;
    protected float durationTimer;

    public override void PlayInstant (GameObject anchorObject = null, float instantMultiplier = 1F) {
        this.anchorObject = anchorObject;
        if (targets.Count == 0) {
            GetTargets();
        }
        if (targets.Count > 0) {
            foreach (RandomShake shake in targets) {
                shake.AddTrauma(instantShake * instantMultiplier);
            }
        }
    }

    public override void PlayToggle (bool toggle, GameObject anchorObject = null) {
        base.PlayToggle(toggle, anchorObject);
        if (toggleState) {
            this.anchorObject = anchorObject;
            if (targets.Count == 0) {
                GetTargets();
            }
        }
        else {
            durationTimer = 0F;
        }
    }

    void Update () {

        if (toggleState) {
            durationTimer += Time.deltaTime;
            foreach (RandomShake shake in targets) {
                shake.AddTrauma(durationShake.Evaluate(durationTimer / durationTime));
            }
        }

    }

    protected void GetTargets () {

        if (anchorObject != null) {
            foreach (RandomShake shake in anchorObject.GetComponentsInChildren<RandomShake>()) {
                targets.Add(shake);
            }
        }

    }

    override public void WipeTargets () {

        targets.Clear();

    }

    public override void Reset () {
        base.Reset();
        durationTimer = 0F;
    }

}