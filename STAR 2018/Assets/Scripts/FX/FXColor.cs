﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXColor : FX {

    protected GameObject anchorObject;
    protected List<SpriteColorizer> targets = new List<SpriteColorizer>();

    public Color instantColor = Color.white;
    public float instantStrength;
    public float instantFadeTime;
    public Gradient durationGradient;
    public float durationStrength;
    public bool durationLoops = false;
    public float durationFadeTime;
    public float durationTime = 1F;
    protected float durationTimer;

    protected static int staticIndividualizer = 0;
    protected int myIndividiualizer = 0;

    protected bool firstUpdate = true;

    void Start () {
        myIndividiualizer = staticIndividualizer;
        staticIndividualizer++;
    }

    public override void PlayInstant (GameObject anchorObject = null, float instantMultiplier = 1F) {
        this.anchorObject = anchorObject;
        if (targets.Count == 0) {
            GetTargets();
        }
        if (targets.Count > 0) {
            foreach (SpriteColorizer sc in targets) {
                sc.AddColorFactor(instantColor, instantFadeTime, instantStrength * instantMultiplier, name + myIndividiualizer, true);
            }
        }
    }

    public override void PlayToggle (bool toggle, GameObject anchorObject = null) {
        base.PlayToggle(toggle, anchorObject);
        if (toggleState) {
            this.anchorObject = anchorObject;
            if (targets.Count == 0) {
                GetTargets();
            }
        }
        else {
            durationTimer = 0F;
        }
    }

    void Update () {

        if (toggleState) {
            durationTimer += Time.deltaTime;
            if (durationLoops) { durationTimer = durationTimer % durationTime; }
            Color c = durationGradient.Evaluate(durationTimer / durationTime);
            c = new Color(c.r, c.g, c.b, 1F);
            float strength = durationStrength * durationGradient.Evaluate(durationTimer / durationTime).a;
            foreach (SpriteColorizer sc in targets) {
                sc.AddColorFactor(c, durationFadeTime, strength, name + myIndividiualizer, true);
            }
        }

    }

    protected void GetTargets () {

        if (anchorObject != null) {
            if (anchorObject.GetComponent<SpriteColorizer>() != null) {
                targets.Add(anchorObject.GetComponent<SpriteColorizer>());
            } 
            foreach (SpriteColorizer sc in anchorObject.GetComponentsInChildren<SpriteColorizer>()) {
                targets.Add(sc);
            }
        }

    }

    public override void Reset () {
        base.Reset();
        durationTimer = 0F;
    }

}
