﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SuperFunctions;

public class FXCamShake : FX {

    public float instantShake = 0.5F;

    public AnimationCurve durationCurve = AnimationCurve.EaseInOut(0F, 0F, 1F, 1F);
    public float durationDecaySpeed = 2F;

    //State: Time
    protected float durationTimer;
    //State: Strength
    protected float curDurationStrength;

    public override void PlayInstant (GameObject anchorObject = null, float instantMultiplier = 1F) {
        CameraShake.instance.AddTrauma(instantShake * instantMultiplier);
    }

    protected override void ToggleGoesOn () {
        durationCurve.keys[0].value = curDurationStrength;
        durationTimer = 0F;
    }

    protected override void ToggleGoesOff () {
        durationCurve.keys[0].value = 0F;
        durationTimer = 0F;
    }

    void Update () {

        if (toggleState) {
            durationTimer += Time.deltaTime * speedMultiplier;
            curDurationStrength = durationCurve.Evaluate(durationTimer);
        } else {
            curDurationStrength = SuperFunctions.GameMath.DecayFloat(curDurationStrength, durationDecaySpeed, Time.deltaTime * speedMultiplier);
        }

        CameraShake.instance.AddTrauma(curDurationStrength * Time.deltaTime);

    }

    public override void Reset () {
        base.Reset();
        durationTimer = 0F;
    }

}
