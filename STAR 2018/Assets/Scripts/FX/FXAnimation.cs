﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class FXAnimation : FX {

    protected List<AnimatedByFX> anControllers = new List<AnimatedByFX>();

    void Awake () {

        foreach (AnimatedByFX a in GetComponentsInChildren<AnimatedByFX>()) {
            anControllers.Add(a);
        }

    }

    public override void PlayInstant (GameObject anchorObject = null, float instantMultiplier = 1F) {
        foreach (AnimatedByFX a in anControllers) {
            if (!a.isSetUp) a.SetUp();
            a.PlayInstant();
        }
    }

    protected override void ToggleGoesOn () {
        foreach (AnimatedByFX a in anControllers) {
            if (!a.isSetUp) a.SetUp();
            a.ToggleOn();
        }
    }

    protected override void ToggleGoesOff () {
        foreach (AnimatedByFX a in anControllers) {
            a.ToggleOff();
        }
    }

}