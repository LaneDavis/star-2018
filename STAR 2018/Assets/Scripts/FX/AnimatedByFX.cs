﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedByFX : MonoBehaviour {

    protected Animator animator;
    [HideInInspector] public bool isSetUp = false;

	void Awake () {

        SetUp();

    }

    public void SetUp () {

        isSetUp = true;
        animator = GetComponent<Animator>();

    }

    public void PlayInstant () {

        animator.SetTrigger("PlayInstant");

    }

    public void ToggleOn () {

        animator.SetTrigger("ToggleOn");

    }

    public void ToggleOff () {

        animator.SetTrigger("ToggleOff");
        
    }

}
