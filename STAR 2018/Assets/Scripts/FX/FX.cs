﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FX : MonoBehaviour {

    //Tuning
    [HideInInspector] public float speedMultiplier = 1F;
    [HideInInspector] public float instantMultiplier = 1F;

    [HideInInspector] public bool toggleState = false;

    void Awake () {
        toggleState = false;
    }

	public virtual void PlayInstant (GameObject anchorObject = null, float instantMultiplier = 1F) {
        //Override this.
    }
    
    public virtual void PlayToggle (bool toggle, GameObject anchorObject = null) {
        if (toggle && !toggleState) {
            toggleState = true; ToggleGoesOn(); 
        } else if (!toggle && toggleState) {
            toggleState = false; ToggleGoesOff();
        }
    }

    protected virtual void ToggleGoesOn () {
        //Override this.
    }

    protected virtual void ToggleGoesOff () {
        //Override this.
    }

    public virtual void WipeTargets () {
        //Override this.
    }

    public virtual void Reset () {
        WipeTargets();
        toggleState = false;
        //Override this. 
    }

}
