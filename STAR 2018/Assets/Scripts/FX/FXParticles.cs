﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class FXParticles : FX {

    public List<ParticleSystem> InstantParticles = new List<ParticleSystem>();
    public List<ParticleSystem> DurationParticles = new List<ParticleSystem>();

    void Start () {
        foreach (ParticleSystem p in InstantParticles) { p.Stop(); p.Clear(); }
        foreach (ParticleSystem p in DurationParticles) { p.Stop(); p.Clear(); }
    }

	public override void PlayInstant (GameObject anchorObject = null, float instantMultiplier = 1F) {
        foreach (ParticleSystem p in InstantParticles) {
            p.Emit((int)(p.emission.rateOverTime.constant * instantMultiplier));
        }
    }

    protected override void ToggleGoesOn () {
        foreach (ParticleSystem p in DurationParticles) {
            p.Play();
        }
    }

    protected override void ToggleGoesOff () {
        foreach (ParticleSystem p in DurationParticles) {
            p.Stop();
        }
    }

    public void SetChildrenAsInstant () {

        InstantParticles.Clear();
        DurationParticles.Clear();
        foreach (ParticleSystem p in GetComponentsInChildren<ParticleSystem>()) {
            InstantParticles.Add(p);
        }

    }

    public void SetChildrenAsDuration () {

        InstantParticles.Clear();
        DurationParticles.Clear();
        foreach (ParticleSystem p in GetComponentsInChildren<ParticleSystem>()) {
            DurationParticles.Add(p);
        }

    }

}

#if UNITY_EDITOR
[CustomEditor(typeof(FXParticles))]
public class SFX_Particles_Editor : Editor {
    public override void OnInspectorGUI () {
        base.OnInspectorGUI();

        FXParticles particlesScript = (FXParticles)target;
        if (GUILayout.Button("Set Children as Instant")) {
            particlesScript.SetChildrenAsInstant();
        }
        if (GUILayout.Button("Set Children as Duration")) {
            particlesScript.SetChildrenAsDuration();
        }
    }
}
#endif