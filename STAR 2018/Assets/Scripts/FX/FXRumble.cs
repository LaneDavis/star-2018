﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXRumble : FX {

    public float instantRumble;
    public AnimationCurve durationRumble;
    public float durationTime = 1F;
    protected float durationTimer;

    [Header("Targeting")]
    public bool targetAnchorPlayer = true;
    protected RumbleController targetRumbleController;
    public float radius = 0F;

    public override void PlayInstant (GameObject anchorObject = null, float instantMultiplier = 1F) {

        SetTarget(anchorObject);
        if (targetRumbleController != null) targetRumbleController.AddRumble(instantRumble * instantMultiplier);
        foreach (PlayerData pd in SuperFunctions.GameMath.playersInRadius(transform.position, radius)) {
            pd.entityData.rumbleController.AddRumble(instantRumble * (1F - Vector2.Distance(transform.position, pd.playerObject.transform.position) / radius) * instantMultiplier);
        }

    }

    public override void PlayToggle (bool toggle, GameObject anchorObject = null) {
        base.PlayToggle(toggle, anchorObject);
        SetTarget(anchorObject);
        durationTimer = 0F;
    }

    void Update () {

        durationTimer += Time.deltaTime;
        if (toggleState) {
            if (targetRumbleController != null) targetRumbleController.AddRumble(instantRumble);
            foreach (PlayerData pd in SuperFunctions.GameMath.playersInRadius(transform.position, radius)) {
                pd.entityData.rumbleController.AddRumble(instantRumble * (1F - Vector2.Distance(transform.position, pd.playerObject.transform.position) / radius));
            }
        }

    }

    public override void Reset () {
        base.Reset();
        durationTimer = 0F;
        targetRumbleController = null;
    }

    void SetTarget (GameObject anchorObject) {
        if (!targetAnchorPlayer) return;
        if (targetRumbleController != null) return;
        if (anchorObject == null) return;
        targetRumbleController = anchorObject.GetComponent<RumbleController>();
    }

}