﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FX_Controller : FX {

    [Header("Anchor")]
    public GameObject anchorObject = null;
    public enum ActionWhenAnchorDies { nothing, toggleOff, destroy, playInstant }
    public ActionWhenAnchorDies actionWhenAnchorDies = ActionWhenAnchorDies.nothing;

    [Header("Startup")]
    public bool instantOnStart = false;
    public bool toggleOnStart = false;

    public bool delayOneFrame = false;

    public bool autoKill = false;
    public float autoKillTime = 8F;
    public bool durationAutoHalt = false;
    public float autoHaltTime = 4F;
    protected float autoHaltTimer = 0F;

    protected float elapsedTime = 0F;

    protected bool firstUpdate = true;

    [Header("Editor Testing")]
    public bool ed_instant;
    public bool ed_toggle;

    void Start () {

        if (instantOnStart && !delayOneFrame) {
            PlayAllInstant(anchorObject);
        }

        if (toggleOnStart && !delayOneFrame) {
            ToggleAll(true, anchorObject);
        }

    }

    void FirstUpdate () {

        firstUpdate = false;

        if (instantOnStart && delayOneFrame) {
            PlayAllInstant(anchorObject);
        }

        if (toggleOnStart && delayOneFrame) {
            ToggleAll(true, anchorObject);
        }

    }

    void Update () {

        if (firstUpdate) FirstUpdate();

        if (autoKill) {
            elapsedTime += Time.deltaTime;
            if (elapsedTime > autoKillTime) {
                CleanUpFX();
            }
        }

        if (toggleState && durationAutoHalt) {
            autoHaltTimer += Time.deltaTime;
            if (autoHaltTimer >= autoHaltTime) {
                ToggleAll(false);
                autoHaltTimer = 0F;
            }
        }

        if (ed_instant) {
            ed_instant = false;
            PlayAllInstant();
        }

        if (ed_toggle) {
            ed_toggle = false;
            ToggleAll(!toggleState, anchorObject);
        }

        if (actionWhenAnchorDies != ActionWhenAnchorDies.nothing) {
            if (anchorObject == null || !anchorObject.activeSelf) {
                if (actionWhenAnchorDies == ActionWhenAnchorDies.destroy) CleanUpFX();
                if (actionWhenAnchorDies == ActionWhenAnchorDies.toggleOff) ToggleAll(false);
                if (actionWhenAnchorDies == ActionWhenAnchorDies.playInstant) {
                    PlayAllInstant();
                    if (GetComponent<ObjectDetacher>() != null) GetComponent<ObjectDetacher>().DetachAndScheduleDeath(10F);
                }
            }
        }

    }

    /// <summary>
    /// Destroys the FX and all child FX gracefully, giving particles (and other detached FX)
    /// a moment to play out before getting destroyed.
    /// </summary>
    public void CleanUpFX () {

        foreach (ParticleSystem p in GetComponentsInChildren<ParticleSystem>()) {
            p.Stop();
        }
        foreach (ObjectDetacher o in GetComponentsInChildren<ObjectDetacher>()) {
            o.DetachAndScheduleDeath(10F);
        }
        GameObject.Destroy(gameObject);

    }

    public void PlayAllInstant (GameObject anchorObject = null, float instantMultiplier = 1f) {
        if (!(this.anchorObject != null && anchorObject == null)) {
            this.anchorObject = anchorObject;
        }
        foreach (FX fx in GetComponents<FX>()) {
            fx.PlayInstant(anchorObject:this.anchorObject, instantMultiplier:instantMultiplier);
        }
    }

    public void ToggleAll (bool toggle, GameObject anchorObject = null) {
        if (!(this.anchorObject != null && anchorObject == null)) {
            this.anchorObject = anchorObject;
        }
        foreach (FX fx in GetComponents<FX>()) {
            fx.PlayToggle(toggle, this.anchorObject);
        }

        if (toggle && durationAutoHalt) {
            autoHaltTimer = 0F;
        }
    }

    public override void WipeTargets () {
        foreach (FX fx in GetComponents<FX>()) {
            if (fx != this)
                fx.WipeTargets();
        }
    }

    public void ScheduleDeath (float seconds) {
        if (autoKill) autoKillTime = elapsedTime + seconds;
        else autoKillTime = seconds;
        autoKill = true;
    }

    public void ResetElapsedTime () {
        elapsedTime = 0f;
    }

}
