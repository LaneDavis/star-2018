﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FXSizePulse : FX {

    protected GameObject anchorObject;
    protected List<SizePulse> targets = new List<SizePulse>();

    public float instantStrength;
    public AnimationCurve durationStrength;
    public float durationTime = 1F;
    protected float durationTimer;

    protected bool firstUpdate = true;

    public override void PlayInstant (GameObject anchorObject = null, float instantMultiplier = 1F) {
        this.anchorObject = anchorObject;
        GetTarget();
        if (targets.Count > 0) {
            foreach (SizePulse target in targets) {
                target.AddSizeFactor(instantStrength * Mathf.Pow(instantMultiplier, 0.5F));
            }
        }
    }

    public override void PlayToggle (bool toggle, GameObject anchorObject = null) {
        base.PlayToggle(toggle, anchorObject);
        if (toggleState) {
            durationTimer = 0F;
            this.anchorObject = anchorObject;
            GetTarget();
        }
    }

    void Update () {

        if (toggleState) {
            durationTimer += Time.deltaTime;
            if (targets.Count > 0) {
                foreach (SizePulse target in targets) {
                    target.AddSizeFactor(durationStrength.Evaluate(durationTimer / durationTime) * Time.deltaTime);
                }
            }
        }

    }

    protected void GetTarget () {

        targets.Clear();
        if (anchorObject != null) {
            foreach (SizePulse sp in anchorObject.GetComponentsInChildren<SizePulse>()) {
                targets.Add(sp);
            }
            if (anchorObject.GetComponent<SizePulse>() != null) {
                targets.Add(anchorObject.GetComponent<SizePulse>());
            }
        }

    }

    public override void Reset () {
        base.Reset();
        durationTimer = 0F;
    }

}
