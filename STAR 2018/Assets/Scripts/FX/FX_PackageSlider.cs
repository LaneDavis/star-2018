﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class FX_PackageSlider : MonoBehaviour {

    protected float curValue;
    protected bool posShift = false;
    public List<SliderPackage> sliderPackages = new List<SliderPackage>();
    protected bool isPaused = false;

    public void SetValue (float newValue, GameObject anchor = null) {

        if (!isPaused) {
            posShift = newValue > curValue;

            foreach (SliderPackage p in PackagesBetweenValues(curValue, newValue)) {
                if (posShift) {
                    if (p.isInstant) p.package.PlayAllInstant(anchor);
                    else p.package.ToggleAll(true, anchor);
                }
                else {
                    if (!p.isInstant) p.package.ToggleAll(false, anchor);
                }
            }

            curValue = newValue;
        }

    }

    public void DeactivateAll () {

        foreach (SliderPackage p in sliderPackages) {
            if (!p.isInstant) p.package.ToggleAll(false, null);
        }

        curValue = 0F;

    }

    protected List<SliderPackage> PackagesBetweenValues (float v1, float v2) {

        List<SliderPackage> returned = new List<SliderPackage>();
        foreach (SliderPackage p in sliderPackages) {
            if (v1 != v2 && (p.threshold >= v1 && p.threshold <= v2 || p.threshold <= v1 && p.threshold >= v2)) {
                returned.Add(p);
            }
        }

        return returned;

    }

    public void Reset () { 
        //Override this. 
    }

    public void CreateBasicSFXChild () {

        GameObject newChild = GameObject.Instantiate(new GameObject(), transform.position, Quaternion.identity, transform);
        newChild.AddComponent<FX_Controller>();
        newChild.name = "[SFX] sfx " + (transform.childCount - 1);

    }

    public void LoadChildSFX () {

        sliderPackages.Clear();
        foreach (FX_Controller fxc in GetComponentsInChildren<FX_Controller>()) {
            SliderPackage newSliderPackage = new SliderPackage();
            newSliderPackage.package = fxc;
            sliderPackages.Add(newSliderPackage);
        }

    }

    public void SetupDefaultRamp () {

        //Setup Duration Ramp
        if (sliderPackages.Count > 0) {
            float div = 1F / (float)(sliderPackages.Count - 1);
            for (int i = 0; i < sliderPackages.Count; i++) {
                sliderPackages[i].threshold = (float)(i) * div;
                sliderPackages[i].isInstant = false;
                sliderPackages[i].package.gameObject.name = "[SFX] Dura Ramp " + i;
            }
        }

    }

    public void SetupCappedRamp () {

        //Setup Instant Intro
        if (sliderPackages.Count > 0) {
            sliderPackages[0].threshold = 0F;
            sliderPackages[0].isInstant = true;
            sliderPackages[0].package.gameObject.name = "[SFX] Inst Start";
        }

        //Setup Instant Finale
        if (sliderPackages.Count > 1) {
            sliderPackages[sliderPackages.Count - 1].threshold = 1F;
            sliderPackages[sliderPackages.Count - 1].isInstant = true;
            sliderPackages[sliderPackages.Count - 1].package.gameObject.name = "[SFX] Inst Finish";
        }

        //Setup Duration Ramp
        if (sliderPackages.Count > 2) {
            float div = 1F / (float)(sliderPackages.Count - 2);
            for (int i = 1; i < sliderPackages.Count - 1; i++) {
                sliderPackages[i].threshold = (float)(i - 1) * div;
                sliderPackages[i].isInstant = false;
                sliderPackages[i].package.gameObject.name = "[SFX] Dura Ramp " + i;
            }
        }

    }

    public void Pause () {
        isPaused = true;
    }

    public void Unpause () {
        isPaused = false;
    }

}

[System.Serializable]
public class SliderPackage {

    public float threshold;
    public FX_Controller package;
    public bool isInstant = true;

}

#if UNITY_EDITOR
[CustomEditor(typeof(FX_PackageSlider))]
public class SFX_Slider_Editor : Editor {
    public override void OnInspectorGUI () {
        base.OnInspectorGUI();

        FX_PackageSlider script = (FX_PackageSlider)target;
        if (GUILayout.Button("Create Basic SFX Child")) {
            script.CreateBasicSFXChild();
        }
        if (GUILayout.Button("Load Child SFX")) {
            script.LoadChildSFX();
        }
        if (GUILayout.Button("Setup Default Ramp")) {
            script.SetupDefaultRamp();
        }
        if (GUILayout.Button("Setup Capped Ramp")) {
            script.SetupCappedRamp();
        }
    }
}
#endif
