﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour {

    public List<GameObject> enemiesChecked = new List<GameObject>();
    public List<ButtonController> buttonsChecked = new List<ButtonController>();

    public float distanceMoved = 17.5f;
    public float openingDuration = 2f;
    public bool manuallyOpenGate;
    private Vector2 closedPosition = Vector2.zero;
    private Vector2 openPosition = Vector2.zero;

    [HideInInspector]
    public float openingPercent = 0f;
    private bool gateIsOpening = false;
    private bool gateIsClosing = true;

    void Start () {
        closedPosition = transform.position;
        openPosition = (Vector2)transform.position + (Vector2)transform.right * distanceMoved;
    }

    void Update () {

        //Each frame, check the enemies. If none are left, open the gate.
        bool shouldOpen = true;
        gateIsClosing = false;
        foreach (GameObject enemy in enemiesChecked) {
            if (enemy != null) shouldOpen = false;
        }
        foreach (ButtonController button in buttonsChecked) {
            if (!button.IsOn) {
                shouldOpen = false;
                gateIsClosing = true;
            }
        }
        if (shouldOpen && openingPercent < 1f) {
            gateIsOpening = true;
        }

        //Add a toggle so we can manually confirm the gate works.
        if (manuallyOpenGate) {
            manuallyOpenGate = false;
            gateIsOpening = true;
        }

        //If the gate is opening, continue moving until it's moved the desired time and distance.
        if (gateIsOpening && openingPercent < 1f) {
            openingPercent += Time.deltaTime / openingDuration;
            if (openingPercent >= 1) {
                gateIsOpening = false;
            }
        }

        if (gateIsClosing) {
            openingPercent = Mathf.Clamp01(openingPercent -= 2.5f * Time.deltaTime / openingDuration);
        }
        
        transform.position = Vector2.Lerp(closedPosition, openPosition, openingPercent);

    }

}
