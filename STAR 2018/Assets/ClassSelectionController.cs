﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Rewired;

public class ClassSelectionController : MonoBehaviour {

    public Animator animator;
    public ClassCardController currentCard;
    public GameObject classCardPrefab;

    public float navigationCooldown = 0.33f;
    private float navigationTimer = 0f;

    private int playerNumber;
    private Player player;
    private List<CharacterClass> characterClasses;
    private int currentClassNum = 0;

    private bool isActive = false;

	public void Setup (int playerNumber, Player player, List<CharacterClass> characterClasses) {

        isActive = true;

        this.playerNumber = playerNumber;
        this.player = player;
        this.characterClasses = characterClasses;

        currentCard.Setup(characterClasses[0]);

        //Animate In
        animator.Play("Selector Animate In");
        NavigateLeft();

    }

    void Update () {
        if (isActive) {

            navigationTimer += Time.deltaTime;
            
            if (navigationTimer > navigationCooldown) {
                if (player.GetAxis("MoveX") < -0.25f) {
                    navigationTimer = 0f;
                    NavigateLeft();
                } else if (player.GetAxis("MoveX") > 0.25f) {
                    navigationTimer = 0f;
                    NavigateRight();
                }
            }

            if (player.GetButtonDown("Shoot") || player.GetButtonDown("MouseAttack")) {
                SpawnPlayerAndExit();
            }

        }
    }

    void NavigateLeft () {

        currentClassNum--;
        if (currentClassNum < 0)
            currentClassNum = characterClasses.Count - 1;

        currentCard.animator.Play("Card Animate Out (to Right)");

        GameObject newCard = GameObject.Instantiate(classCardPrefab, transform.position, Quaternion.identity, transform);
        currentCard = newCard.GetComponent<ClassCardController>();
        currentCard.Setup(characterClasses[currentClassNum]);
        currentCard.animator.Play("Card Animate In (from Left)");

    }

    void NavigateRight () {

        currentClassNum++;
        if (currentClassNum >= characterClasses.Count)
            currentClassNum = 0;

        currentCard.animator.Play("Card Animate Out (to Left)");

        GameObject newCard = GameObject.Instantiate(classCardPrefab, transform.position, Quaternion.identity, transform);
        currentCard = newCard.GetComponent<ClassCardController>();
        currentCard.Setup(characterClasses[currentClassNum]);
        currentCard.animator.Play("Card Animate In (from Right)");

    }

    void SpawnPlayerAndExit () {
        isActive = false;
        animator.Play("Selector Choose and Animate Out");
        PlayerManager.instance.SpawnPlayer(playerNumber, characterClasses[currentClassNum]);
    }

}
