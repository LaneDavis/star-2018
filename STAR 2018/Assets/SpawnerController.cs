﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour {

    [HideInInspector]
    public GameObject targetPlayer;
    public SpriteColorizer colorizer;

    private float tickLength = 0.25F; 
    private float tickTimer = 0F;

    public GameObject enemyPrefab;
    private List<GameObject> activeEnemies = new List<GameObject>();

    public int maxEnemies = 2;
    public float spawnCooldown = 1f;
    private float spawnTimer = 0f;

    public float awakeDistance = 15F;
    public float sleepDistance = 25F;

    public Gradient colorPattern;
    public float colorSpeed;
    private float colorTimer;

    private void Update () { 

        //Update Tick (performs occasional actions)
        tickTimer += Time.deltaTime;
        if (tickTimer > tickLength) Tick();

        colorTimer += Time.deltaTime * colorSpeed;
        colorizer.AddColorFactor(colorPattern.Evaluate(colorTimer % 1f), 0.25f, 1f, "pattern");

        if (targetPlayer != null) {
            Debug.Log("Spawner Has Target");
            if (activeEnemies.Count < maxEnemies) {
                Debug.Log("SpawnTimer");
                spawnTimer += Time.deltaTime;
                if (spawnTimer > spawnCooldown) {
                    Debug.Log("Spawner Making Enemy");
                    spawnTimer = 0f;
                    GameObject newEnemy = Instantiate(enemyPrefab, transform.position, Quaternion.identity, null);
                    activeEnemies.Add(newEnemy);
                    colorizer.AddColorFactor(Color.white, 0.5f, 2f, "Spawn", true);
                }
            }
        }

    }

    void Tick () {
        Debug.Log("Spawner Tick");
        for (int i = activeEnemies.Count - 1; i >= 0; i--) {
            if (activeEnemies[i] == null) {
                activeEnemies.RemoveAt(i);
            }
        }
        tickTimer = 0F;
        AcquireTarget();
    }

    void AcquireTarget () {
        PlayerData nearestPlayer = PlayerManager.instance.NearestPlayerToPoint(transform.position);
        if (nearestPlayer != null) {
            if (Vector2.Distance(transform.position, nearestPlayer.playerObject.transform.position) > awakeDistance)
                if (targetPlayer == null)
                    return;
                else {
                    if (Vector2.Distance(transform.position, nearestPlayer.playerObject.transform.position) > sleepDistance) {
                        targetPlayer = null;
                    }
                }
            else
                targetPlayer = nearestPlayer.playerObject;
        }
    }

}
