﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathController : MonoBehaviour {
    
    [HideInInspector] public EntityData entityData;
    public FX_Controller deathFX;
    public ObjectDetacher deathFXDetacher;

    public delegate void Death (EntityData entityData);
    public event Death OnDeath;

    public void Die () {

        if (deathFX != null) {
            deathFX.PlayAllInstant();
            deathFXDetacher.DetachAndScheduleDeath(10F);
        }

        if (entityData.IsAnEnemy()) {
            MoneyManager.Instance.EarnGold(entityData.goldReward);
            EnemyManager.instance.RemoveEnemy(entityData);
        }

        if (OnDeath != null) {
            OnDeath(entityData);
        }

        Destroy(gameObject);

    }

}
